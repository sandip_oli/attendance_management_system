package com.phonebook.core.api.impl;

import com.phonebook.core.api.iapi.IRoleApi;
import com.phonebook.core.model.converter.RoleConverter;
import com.phonebook.core.model.dto.RoleDTO;
import com.phonebook.core.model.entity.Role;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.repository.RoleRepository;
import com.phonebook.core.util.ConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleApi implements IRoleApi{

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleConverter roleConverter;

    @Override
    public RoleDTO save(RoleDTO roleDTO) {

        Role role = roleConverter.convertToEntity(roleDTO);

        role.setStatus(Status.ACTIVE);

        return roleConverter.convertToDto(roleRepository.save(role));
    }

    @Override
    public RoleDTO getByTitle(String title) {
        return roleConverter.convertToDto(roleRepository.findByTitle(title.trim()));
    }

    @Override
    public RoleDTO show(long roleId, Status status) {
        return roleConverter.convertToDto(roleRepository.findByIdAndStatus(roleId , status));
    }

    @Override
    public List<RoleDTO> list(Status status) {

        Pageable pageable = ConvertUtil.createPageRequest(0 , 100 , "title" , Sort.Direction.ASC);

        return roleConverter.convertToDtoList(roleRepository.findAllByStatus(status , pageable));
    }

    @Override
    public RoleDTO update(RoleDTO roleDTO) {

        Role role = roleRepository.findByIdAndStatus(roleDTO.getRoleId() , Status.ACTIVE);

        role = roleConverter.copyConvertToEntity(roleDTO , role);

        return roleConverter.convertToDto(roleRepository.save(role));
    }
}
