package com.phonebook.core.api.impl;

import com.phonebook.core.api.iapi.ISendMailSSL;
import com.phonebook.web.util.LoggerUtil;
import com.phonebook.web.util.ParameterConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Date;
import java.util.Properties;

/**
 * Created by dhiraj on 1/18/18.
 */
@Service
public class SendMailSSL implements ISendMailSSL {


    @Autowired
    private TaskExecutor taskExecutor;

    private Properties getProperty() {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        return props;
    }

    private Session getSesseion(Properties props) {

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(ParameterConstants.MAIL_USERNAME, ParameterConstants.MAIL_PASSWORD);
                    }
                });

        return session;
    }

    @Override
    public void sendMail(String from, String to, String msg, String sub) {

        taskExecutor.execute( new Runnable() {
            public void run() {
                simpleMailHalper(from , to , msg , sub);
            }
        });

    }

    private void simpleMailHalper(String from, String to, String msg, String sub) {

        try {

            Thread.sleep(5000);
            Properties properties = getProperty();

            Session session = getSesseion(properties);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject(sub);
            message.setText(msg);

            try {
                Transport.send(message);
            } catch (Exception e) {
                //e.printStackTrace();
                LoggerUtil.logException(this.getClass() , e);
            }

            System.out.println("Done");

        } catch (AddressException e) {
           // e.printStackTrace();
            //System.out.println("Error: " + e.getMessage());

            LoggerUtil.logWarn(this.getClass() , e);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            LoggerUtil.logException(this.getClass() , e);
        }
    }

    @Override
    public void sendHtmlMail(String from, String to, String msg, String sub) {

        try {

            taskExecutor.execute(
                    new Runnable() {
                        public void run() {
                            htmlMailHelper(from, to, msg, sub);
                        }
                    });
        }catch (Exception e){
            LoggerUtil.logWarn(this.getClass() , e);
        }

    }

    private void htmlMailHelper(String from, String to, String msg, String sub) {

        try {

            if (from == null || to == null || msg == null || sub == null){
                return;
            }

            Thread.sleep(5000);

            Properties properties = getProperty();

            Session session = getSesseion(properties);

            Message message = new MimeMessage(session);
            Multipart multiPart = new MimeMultipart("alternative");

            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setText(sub, "utf-8");

            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(msg, "text/html; charset=utf-8");

            multiPart.addBodyPart(htmlPart);
            message.setContent(multiPart);

            if (from != null) {
                message.setFrom(new InternetAddress(from));
            } else
                message.setFrom();

            InternetAddress[] toAddresses = {new InternetAddress(to)};
            message.setRecipients(Message.RecipientType.TO, toAddresses);
            message.setSubject(sub);
            message.setSentDate(new Date());

            Transport.send(message);

        } catch (AddressException e) {

            LoggerUtil.logWarn(this.getClass() , e);

        } catch (MessagingException e) {

            LoggerUtil.logWarn(this.getClass() , e);

        } catch (InterruptedException e) {
            LoggerUtil.logException(this.getClass() , e);
        } finally {
            System.out.println("Email sent! to : " + to + " : by : " + from);
            LoggerUtil.logInfo(this.getClass() ,"Email sent! to : " + to + " : by : " + from);
        }
    }

}

