package com.phonebook.core.api.impl;

import com.phonebook.core.api.iapi.ICountryInfoApi;
import com.phonebook.core.model.converter.CountryInfoConverter;
import com.phonebook.core.model.dto.CountryInfoDTO;
import com.phonebook.core.model.entity.CountryInfo;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.repository.CountryInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dhiraj on 4/18/17.
 */
@Service
public class CountryInfoApi implements ICountryInfoApi {

    @Autowired
    private CountryInfoConverter countryConverter;

    @Autowired
    private CountryInfoRepository countryRepository;


    @Override
    public CountryInfoDTO save(CountryInfoDTO countryDTO) {

        return countryConverter.convertToDto(countryRepository.save(countryConverter.convertToEntity(countryDTO)));
    }

    @Override
    public CountryInfoDTO update(CountryInfoDTO countryDTO) {

        return countryConverter.convertToDto(countryRepository.save(countryConverter.copyConvertToEntity(countryDTO , countryRepository.findByIdAndStatus(countryDTO.getCountryId() , Status.ACTIVE))));
    }

    @Override
    public void delete(long countryId) {

        CountryInfo country = countryRepository.findByIdAndStatus(countryId, Status.ACTIVE);

        country.setStatus(Status.DELETED);

        countryRepository.save(country);

    }

    @Override
    public CountryInfoDTO show(long countryId) {

        return countryConverter.convertToDto(countryRepository.findByIdAndStatus(countryId , Status.ACTIVE));
    }

    @Override
    public CountryInfoDTO getByName(String name) {
        return countryConverter.convertToDto(countryRepository.findByName(name));
    }

    @Override
    public List<CountryInfoDTO> list() {

        return countryConverter.convertToDtoList(countryRepository.findAllByStatus(Status.ACTIVE));
    }
}
