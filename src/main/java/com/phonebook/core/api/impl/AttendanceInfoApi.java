package com.phonebook.core.api.impl;


import com.phonebook.core.api.iapi.IStudentAttendenceApi;
import com.phonebook.core.model.converter.AttendanceInfoConverter;
import com.phonebook.core.model.dto.AttendanceInfoDto;
import com.phonebook.core.model.entity.AttendanceInfo;
import com.phonebook.core.model.repository.AttendanceInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class AttendanceInfoApi implements IStudentAttendenceApi {

    @Autowired
    private AttendanceInfoRepository attendanceInfoRepository;

    @Autowired
    private AttendanceInfoConverter attendanceInfoConverter;

    @Override
    public AttendanceInfoDto save(AttendanceInfoDto dto) {
        AttendanceInfo entity = attendanceInfoConverter.convertToEntity(dto);
        entity = attendanceInfoRepository.save(entity);

        return attendanceInfoConverter.convertToDto(entity);
    }

    @Override
    public AttendanceInfoDto update(AttendanceInfoDto dto) {
        AttendanceInfo entity = attendanceInfoRepository.findOne(dto.getAttendanceInfoId());
        entity = attendanceInfoConverter.copyConvertToEntity(dto , entity);
        entity = attendanceInfoRepository.save(entity);

        return attendanceInfoConverter.convertToDto(entity);
    }


    @Override
    public AttendanceInfoDto show(Long studentInfoId) {
        AttendanceInfo entity = attendanceInfoRepository.findOne(studentInfoId);
        return attendanceInfoConverter.convertToDto(entity);
    }

    @Override
    public List<AttendanceInfoDto> list() {
        List<AttendanceInfo> studentInfoList = attendanceInfoRepository.findAll();

        return attendanceInfoConverter.convertToDtoList(studentInfoList);
    }

    @Override
    public List<AttendanceInfoDto> getAllByStudentId(long studentId){
        return attendanceInfoConverter.convertToDtoList(attendanceInfoRepository.findAllByStudentInfo_Id(studentId));
    }

    @Override
    public List<AttendanceInfoDto> getAllByDateBetween(Date from , Date to){
        return attendanceInfoConverter.convertToDtoList(attendanceInfoRepository.findAllByDateBetween(from , to));
    }
}
