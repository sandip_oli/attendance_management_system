package com.phonebook.core.api.impl;

import com.phonebook.core.api.iapi.IStudentInfoApi;
import com.phonebook.core.model.converter.StudentInfoConverter;
import com.phonebook.core.model.dto.StudentInfoDto;
import com.phonebook.core.model.entity.AttendanceInfo;
import com.phonebook.core.model.entity.StudentInfo;
import com.phonebook.core.model.repository.AttendanceInfoRepository;
import com.phonebook.core.model.repository.StudentInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentInfoApi implements IStudentInfoApi {

    @Autowired
    private StudentInfoRepository studentInfoRepository;

    @Autowired
    private StudentInfoConverter studentInfoConverter;

    @Autowired
    private AttendanceInfoRepository attendanceInfoRepository;

    @Override
    public StudentInfoDto save(StudentInfoDto dto) {
        StudentInfo entity = studentInfoConverter.convertToEntity(dto);
        entity = studentInfoRepository.save(entity);

        return studentInfoConverter.convertToDto(entity);
    }

    @Override
    public StudentInfoDto update(StudentInfoDto dto) {
        StudentInfo entity = studentInfoRepository.findOne(dto.getStudentId());
        entity = studentInfoConverter.copyConvertToEntity(dto , entity);
        entity = studentInfoRepository.save(entity);

        return studentInfoConverter.convertToDto(entity);
    }

    @Override
    public StudentInfoDto show(Long studentInfoId) {
        StudentInfo entity = studentInfoRepository.findOne(studentInfoId);
        return studentInfoConverter.convertToDto(entity);
    }

    @Override
    @Transactional
    public void delete(Long studentInfoId) {
        attendanceInfoRepository.deleteAllByStudentInfo_Id(studentInfoId);
        studentInfoRepository.delete(studentInfoId);

    }

    @Override
    public List<StudentInfoDto> list() {

        List<StudentInfo> studentInfoList = studentInfoRepository.findAll();

        return studentInfoConverter.convertToDtoList(studentInfoList);
    }
}
