package com.phonebook.core.api.impl;

import com.phonebook.core.model.dto.StateInfoDTO;
import com.phonebook.core.api.iapi.IStateInfoApi;
import com.phonebook.core.model.converter.StateInfoConverter;
import com.phonebook.core.model.entity.StateInfo;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.repository.StateInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dhiraj on 4/25/17.
 */
@Service
public class StateInfoApi implements IStateInfoApi {

    @Autowired
    private StateInfoRepository stateRepository;

    @Autowired
    private StateInfoConverter stateConverter;

    @Override
    public StateInfoDTO save(StateInfoDTO stateDTO) {

        return stateConverter.convertToDto(stateRepository.save(stateConverter.convertToEntity(stateDTO)));
    }

    @Override
    public StateInfoDTO update(StateInfoDTO stateDTO) {

        return stateConverter.convertToDto(stateRepository.save(stateConverter.copyConvertToEntity(stateDTO , stateRepository.findByIdAndStatus(stateDTO.getStateId() , Status.ACTIVE))));
    }

    @Override
    public void delete(long stateId) {

        StateInfo state = stateRepository.findByIdAndStatus(stateId , Status.ACTIVE);

        state.setStatus(Status.DELETED);

        stateRepository.save(state);
    }

    @Override
    public StateInfoDTO show(long stateId) {

        return stateConverter.convertToDto(stateRepository.findByIdAndStatus(stateId , Status.ACTIVE));
    }

    @Override
    public StateInfoDTO getByName(String stateName) {
        return stateConverter.convertToDto(stateRepository.findByName(stateName));
    }

    @Override
    public List<StateInfoDTO> list() {

        return stateConverter.convertToDtoList(stateRepository.findAllByStatus(Status.ACTIVE));
    }

	@Override
	public StateInfoDTO getStateByName(String stateName) {
		return stateConverter.convertToDto(stateRepository.findByNameAndStatus(stateName , Status.ACTIVE));
	}
}
