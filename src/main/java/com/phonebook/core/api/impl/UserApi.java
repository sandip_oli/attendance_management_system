package com.phonebook.core.api.impl;

import com.phonebook.core.api.iapi.ISendMailSSL;
import com.phonebook.core.api.iapi.IUserApi;
import com.phonebook.core.model.converter.UserConverter;
import com.phonebook.core.model.dto.UserDTO;
import com.phonebook.core.model.entity.PasswordReset;
import com.phonebook.core.model.entity.User;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.repository.PasswordResetRepository;
import com.phonebook.core.model.repository.RoleRepository;
import com.phonebook.core.model.repository.UserRepository;
import com.phonebook.core.util.ConvertUtil;
import com.phonebook.core.util.MailTemplateUtls;
import com.phonebook.web.util.LoggerUtil;
import com.phonebook.web.util.MailConstants;
import com.phonebook.web.util.ParameterConstants;
import com.phonebook.web.util.RequestUtils;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class UserApi implements IUserApi {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserConverter userConverter;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private ISendMailSSL sendMailSSL;

	@Autowired
	private PasswordResetRepository passwordResetRepository;

	@Override
	public UserDTO save(UserDTO userDTO, HttpServletRequest request)
			throws JSONException {

		userDTO.setEnabled(false);
		User user = userConverter.convertToEntity(userDTO);

		user.setPassword(passwordEncoder.encode(userDTO.getPassword().trim()));
		user.setStatus(Status.ACTIVE);
		user.setActivationCode(getActivationCode());

		user = userRepository.save(user);

		sendMail(user.getUsername(), user.getActivationCode(), request);

		return userConverter.convertToDto(user);
	}

	private void sendMail(String user_name, String code,
			HttpServletRequest request) {
		try {

			String msg = MailTemplateUtls.officialMailTemplate(
					MailConstants.ACTIVATION_MAIL_TITLE,
					MailConstants.ACTIVATION_MAIL_TITLE,
					getActivationCodeMailBody(code, RequestUtils.getServerUlr(request)),
					MailConstants.ACTIVATION_MAIL_TITLE);
			sendMailSSL.sendHtmlMail(ParameterConstants.MAIL_USERNAME,
					user_name, msg, MailConstants.USER_ACTIVATION_SUBJECT);
		} catch (Exception e) {
			LoggerUtil.logException(this.getClass(), e);
		}
	}

	private String getActivationCodeMailBody(String code, String url) {
		String msg = "";

		url = url + "/user/activate?token=" + code;

		msg = "To activate your account <a href='"
				+ url
				+ "' style='border-color: #367fa9; border-radius: 3px;'>click here</a>";

		return msg;
	}

	private String getResetPassCodeMailBody(String code, String username,
			String url) {
		String msg = "";

		url = url + "/user/resetpass?token="
					+ code + "&username=" + username;

		msg = "To reset password <a href='"
				+ url
				+ "' style='border-color: #367fa9; border-radius: 3px;'>click here</a>";

		return msg;
	}

	private String getActivationCode() {
		String code = ConvertUtil.getOrderNo(6);

		while (userRepository.findByActivationCode(code) != null) {
			code = ConvertUtil.getOrderNo(6);
		}

		return code;
	}

	@Override
	public UserDTO update(UserDTO userDTO) throws IOException,
			JSONException {

		User user = userRepository.findOne(userDTO.getUserId());

		user.setUserType(userDTO.getUserType());

		user.setRoleSet(roleRepository.findAllByStatusAndIdIn(Status.ACTIVE,
				userDTO.getRoleIdSet()));

		/*if (userDTO.getUserType().equals(UserType.DATAENTRY)) {
			user.setBranch(branchRepository.findById(userDTO.getBranchId()));
		} else {
			user.setBranch(null);
		}*/

		return userConverter.convertToDto(userRepository.save(user));
	}

	@Override
	public List<UserDTO> list(int page, int size) {

		Pageable pageable = ConvertUtil.createPageRequest(page, size, "id",
				Sort.Direction.DESC);

		return userConverter.convertToDtoList(userRepository
				.findAllUser(pageable));
	}

	@Override
	public List<UserDTO> listWithRole(int page, int size) {

		Pageable pageable = ConvertUtil.createPageRequest(page, size, "id",
				Sort.Direction.DESC);

		return userConverter.convertToDtoListWithRoleAndBranch(userRepository
				.findAllUserJoinRole(pageable));
	}

	@Override
	public long countList() {

		return userRepository.countAll();
	}

	public void changePassword(long userId, String newPassword)
			throws IOException, JSONException {

		User user = userRepository.findOne(userId);

		user.setPassword(passwordEncoder.encode(newPassword));

		userRepository.save(user);
	}

	@Override
	@Transactional
	public void changePassword(long userId, String newPassword,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, JSONException {
		changePassword(userId, newPassword);

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();

		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
	}

	@Override
	public UserDTO getUserByUserName(String userName) {

		User user = userRepository.findByUsername(userName);

		return userConverter.convertToDto(user);
	}

	@Override
	public boolean nameExists(String userName) {

		return userRepository.findByUsername(userName) != null;
	}

	@Override
	public UserDTO show(long userId) {
		return userConverter.convertToDtoWithRoleAndBranch(userRepository
				.findByIdJoinRole(userId));
	}

	@Override
	public UserDTO getById(long userId) {
		return userConverter.convertToDto(userRepository.findOne(userId));
	}

	@Override
	public void enable(long userId, boolean enable) {

		User user = userRepository.findOne(userId);

		user.setEnabled(enable);

		userRepository.save(user);
	}

	@Override
	public void lock(long userId, boolean lock) {

		User user = userRepository.findOne(userId);

		if (lock) {
			user.setStatus(Status.INACTIVE);
		} else {
			user.setStatus(Status.ACTIVE);
		}

		userRepository.save(user);
	}

	@Override
	public void activateUser(String token) {
		User u = userRepository.findByActivationCode(token);
		u.setEnabled(true);
		userRepository.save(u);
	}

	@Override
	public synchronized void resetPasswordComposeCode(String email,
			HttpServletRequest request) {
		User u = userRepository.findByUsername(email);
		PasswordReset pr = new PasswordReset();

		String code = ConvertUtil.getOrderNo(8);

		while (userRepository.findByActivationCode(code) != null) {
			code = ConvertUtil.getOrderNo(8);
		}
		pr.setPasswordResetCode(code);
		pr.setPasswordResetCodeStatus(true);
		pr.setPasswordResetRequestDate(new Date());
		pr.setUser(u);
		passwordResetRepository.save(pr);
		sendMailResetPass(u.getUsername(), pr.getPasswordResetCode(), request);

	}

	private PasswordReset checkPasswordReset(String code) {

		return passwordResetRepository.findByPasswordResetCode(code);
	}

	
	@Override
	public void changePasswordReset(String code, String password) {
		PasswordReset pr = passwordResetRepository
				.findByPasswordResetCode(code);
		pr.setPasswordResetCodeStatus(false);
		User u = userRepository.findByIdJoinRole(pr.getUser().getId());
		u.setPassword(passwordEncoder.encode(password.trim()));
		userRepository.save(u);
	}

	private void sendMailResetPass(String user_name, String code,
			HttpServletRequest request) {
		try {

			String msg = MailTemplateUtls.officialMailTemplate(
					MailConstants.RESETPASS_MAIL_TITLE,
					MailConstants.RESETPASS_MAIL_TITLE,
					getResetPassCodeMailBody(code, user_name,
							RequestUtils.getServerUlr(request)),
					MailConstants.RESETPASS_MAIL_TITLE);
			sendMailSSL.sendHtmlMail(ParameterConstants.MAIL_USERNAME,
					user_name, msg, MailConstants.RESETPASS_MAIL_TITLE);
		} catch (Exception e) {
			LoggerUtil.logException(this.getClass(), e);
		}
	}

}
