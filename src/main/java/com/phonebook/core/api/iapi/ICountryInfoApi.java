package com.phonebook.core.api.iapi;

import com.phonebook.core.model.dto.CountryInfoDTO;

import java.util.List;

public interface ICountryInfoApi {

    CountryInfoDTO save(CountryInfoDTO countryDTO);

    CountryInfoDTO update(CountryInfoDTO countryDTO);

    void delete(long countryId);

    CountryInfoDTO show(long countryId);

    CountryInfoDTO getByName(String name);

    List<CountryInfoDTO> list();

}
