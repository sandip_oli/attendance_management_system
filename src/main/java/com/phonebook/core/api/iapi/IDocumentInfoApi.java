package com.phonebook.core.api.iapi;

import com.phonebook.core.model.dto.DocumentInfoDTO;
import com.phonebook.core.model.entity.DocumentInfo;

import java.util.List;

public interface IDocumentInfoApi {

    DocumentInfo saveAndGetEntity(DocumentInfoDTO documentInfoDTO);

    List<DocumentInfo> saveListAndGetEntity(List<DocumentInfoDTO> documentInfoDTOList);

    DocumentInfoDTO save(DocumentInfoDTO documentInfoDTO);

    void save(long documentInfoId);
}
