package com.phonebook.core.api.iapi;

import com.phonebook.core.model.dto.AttendanceInfoDto;
import com.phonebook.core.model.dto.StudentInfoDto;

import java.util.Date;
import java.util.List;

public interface IStudentAttendenceApi {
    AttendanceInfoDto save(AttendanceInfoDto dto);

    AttendanceInfoDto update(AttendanceInfoDto dto);

    AttendanceInfoDto show(Long studentInfoId);

    List<AttendanceInfoDto> list();

    List<AttendanceInfoDto> getAllByStudentId(long studentId);

    List<AttendanceInfoDto> getAllByDateBetween(Date from, Date to);
}
