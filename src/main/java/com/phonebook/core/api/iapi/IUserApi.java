package com.phonebook.core.api.iapi;

import com.phonebook.core.model.dto.UserDTO;

import org.json.JSONException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;


public interface IUserApi {

    UserDTO save(UserDTO userDTO, HttpServletRequest request) throws JSONException;

    UserDTO update(UserDTO userDTO) throws IOException, JSONException;

	List<UserDTO> list(int page, int size);

    List<UserDTO> listWithRole(int page, int size);

    long countList();

	void changePassword(long userId, String newPassword) throws IOException , JSONException;

	void changePassword(long userId, String newPassword , HttpServletRequest request, HttpServletResponse response) throws IOException , JSONException;

	UserDTO getUserByUserName(String userName);

	boolean nameExists(String userName);

	UserDTO show(long userId);

	UserDTO getById(long userId);

	void enable(long userId , boolean enable);

	void lock(long userId , boolean lock);

	void activateUser(String token);

	void resetPasswordComposeCode(String email, HttpServletRequest request);

	void changePasswordReset(String code, String password);
	
}
