package com.phonebook.core.api.iapi;

public interface ISendMailSSL {
    void sendMail(String from, String to, String msg, String sub);

    void sendHtmlMail(String from, String to, String msg, String sub);
}
