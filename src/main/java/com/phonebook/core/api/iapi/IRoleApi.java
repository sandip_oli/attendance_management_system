package com.phonebook.core.api.iapi;

import com.phonebook.core.model.dto.RoleDTO;
import com.phonebook.core.model.enumconstant.Status;

import java.util.List;

public interface IRoleApi {

    RoleDTO save(RoleDTO roleDTO);

    RoleDTO update(RoleDTO roleDTO);

    RoleDTO getByTitle(String title);

    RoleDTO show(long roleId , Status status);

    List<RoleDTO> list(Status status);

}
