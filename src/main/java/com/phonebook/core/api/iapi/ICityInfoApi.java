package com.phonebook.core.api.iapi;

import com.phonebook.core.model.dto.CityInfoDTO;
import com.phonebook.core.model.dto.PageableDTO;
import com.phonebook.core.model.enumconstant.Status;

import java.util.List;

public interface ICityInfoApi {

	CityInfoDTO save(CityInfoDTO cityDTO);

	CityInfoDTO update(CityInfoDTO cityDTO);

	void delete(long cityId);

	CityInfoDTO show(long cityId);

	List<CityInfoDTO> list(PageableDTO pageableDTO);

	List<CityInfoDTO> list();

	List<CityInfoDTO> list(int max, int offset , String direction , String property);

	CityInfoDTO getCityByName(String cityName);
	
	List<CityInfoDTO> getCityByStateId(long id);

	long cityCount(Status status);

}

