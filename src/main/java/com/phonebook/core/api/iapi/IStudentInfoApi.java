package com.phonebook.core.api.iapi;

import com.phonebook.core.model.dto.StudentInfoDto;

import java.util.List;

public interface IStudentInfoApi {
    StudentInfoDto save(StudentInfoDto dto);

    StudentInfoDto update(StudentInfoDto dto);

    StudentInfoDto show(Long studentInfoId);

    void delete(Long studentInfoId);

    List<StudentInfoDto> list();
}
