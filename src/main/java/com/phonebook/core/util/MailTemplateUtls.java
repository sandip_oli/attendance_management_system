package com.phonebook.core.util;

import com.phonebook.web.util.ParameterConstants;

import java.util.Arrays;

public class MailTemplateUtls {

    public static final String exceptionTemplate(String className , Exception exception){

        //System.out.println(ParameterConstants.EXCEPTION_MAIL_TEMPLATE_PATH);

        String template = new FileHandler().readFileToString(ParameterConstants.EXCEPTION_MAIL_TEMPLATE_PATH);

        template = template.replace("{className}" , className);
        template = exception.getClass() != null ? template.replace("{exceptionClass}"  , exception.getClass().toString()) : template.replace("{exceptionClass}"  , "exceptionClass");
        if (exception.getLocalizedMessage() != null) {
            template = template.replace("{localizedmessage}", exception.getLocalizedMessage());
        }
        if (exception.getCause() != null) {
            template = template.replace("{cause}" , exception.getCause().toString());
        }
        template = template.replace("{trace}" , Arrays.toString(exception.getStackTrace()));

        return template;
    }
    
    public static final String officialMailTemplate(String title , String header,   String body, String footer){

        //System.out.println(ParameterConstants.EXCEPTION_MAIL_TEMPLATE_PATH);

        String template = new FileHandler().readFileToString(ParameterConstants.OFFICIAL_MAIL_TEMPLATE_PATH);

        template = template.replace("{title}" , title).replace("{header}"  , header).replace("{bodycontent}" , body).replace("{footer}" , footer);

        return template;
    }
}
