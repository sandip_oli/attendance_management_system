package com.phonebook.core.util;

import com.phonebook.core.model.entity.DocumentInfo;
import com.phonebook.core.model.enumconstant.DIR;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.web.util.LoggerUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.stream.Stream;

public class FileHandler {

    public String multipartSingleNameResolver(MultipartFile file) {

        if (file != null) {
            if (!file.getOriginalFilename().trim().equals("")) {
                return file.getOriginalFilename();
            } else {
                return "";
            }
        } else {
            return "";
        }

    }

    public static final DocumentInfo uploadClientDoc(MultipartFile image) {

        String randomName = "doc-";

        randomName = randomName + UUID.randomUUID() + "." + image.getContentType().substring(image.getContentType().lastIndexOf("/") + 1);

        String path = FilePath.getOSPath() + File.separator + DIR.CLIENTDOC.getValue();

        File file = new File(path);

        if (!file.exists())
            file.mkdirs();

        LoggerUtil.logInfo(FileHandler.class, "image uploading..............." + path);

        try {
            FileCopyUtils.copy(image.getBytes(), new File(path + File.separator + randomName));
        } catch (Exception e) {
            LoggerUtil.logException(FileHandler.class, e);
            return null;
        }
        LoggerUtil.logInfo(FileHandler.class, "image uploaded..............." + path);

        DocumentInfo documentInfo = new DocumentInfo();

        documentInfo.setTitle("Document");
        documentInfo.setSize(Math.toIntExact(image.getSize()));
        documentInfo.setName(randomName);
        documentInfo.setDir(DIR.CLIENTDOC);
        documentInfo.setStatus(Status.ACTIVE);

        return documentInfo;
    }


    public static boolean delete(String file){
        File doc = new File(file);

        boolean isDeleted = false;
        if (doc.exists()){
            isDeleted =  doc.delete();
        }

        return isDeleted;
    }


    public static StringBuilder readFile(String path) {
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines(Paths.get(path), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            LoggerUtil.logException(FileHandler.class, e);
            return null;
        }

        LoggerUtil.logInfo("file readed : " + contentBuilder.toString());
        return contentBuilder;
    }


    public String readFileToString(String filePath){
        try {

            ClassLoader classLoader = getClass().getClassLoader();

            return FileUtils.readFileToString(new File(classLoader.getResource(filePath).getFile()));
        } catch (IOException e) {
            LoggerUtil.logException(FileHandler.class , e);
            return "";
        }
    }

}
