package com.phonebook.core.util;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.Date;
import java.util.Locale;

public class DateParseUtil {

    private static final String dateFormat = "yyyy-MM-dd";
    private static final String currentDateValue = "01";

    private static final String[] monthValue = {"January" , "February" , "March" , "April" , "May" , "June" , "July" , "August" , "September" , "October" , "November" , "December"};

    public static String getCurrentDate(){
        LocalDate localDate = new LocalDate();

        return localDate.toString(dateFormat , Locale.ENGLISH);
    }

    public static Date parseDate(String date){

        DateTime resultDate = new DateTime(date);

        return resultDate.toDate();
    }

    public static String parseDate(Date date){

        DateTime resultDate = new DateTime(date);

        return resultDate.toString(dateFormat , Locale.ENGLISH);
    }

    public static String getCurrentYearDateStr(){
        String currentDate = getCurrentDate();
        return getYear(currentDate) + "-" + currentDateValue + "-" + currentDateValue;
    }

    public static Date getCurrentYearDate(){
        String currentDate = getCurrentDate();
        return parseDate(getYear(currentDate) + "-" + currentDateValue + "-" + currentDateValue);
    }

    public static Date getYearDate(Date date){
        String currentDate = parseDate(date);
        return parseDate(getYear(currentDate) + "-" + currentDateValue + "-" + currentDateValue);
    }

    public static String getCurrentMonthDateString(){
        String currentDate = getCurrentDate();
        return getYear(currentDate) + "-" + getMonth(currentDate) + "-" + currentDateValue;
    }

    public static String getMonthDateString(Date date){
        String dateStr = parseDate(date);
        return getYear(dateStr) + "-" + getMonth(dateStr) + "-" + currentDateValue;
    }

    public static Date getMonthDate(Date date){
        return parseDate(getMonthDateString(date));
    }

    public static int getMonth(String date){

        DateTime currentDate = new DateTime(date);

        return currentDate.getMonthOfYear();
    }

    public static int getMonth(Date date){

        DateTime currentDate = new DateTime(date);

        return currentDate.getMonthOfYear();
    }

    public static final Date addHours(Date date , int hours){
        DateTime dateTime = new DateTime(date);
        dateTime = dateTime.plusHours(hours);

        return dateTime.toDate();
    }

    public static int getYear(String date){

        DateTime currentDate = new DateTime(date);

        return currentDate.getYear();
    }

    public static String getMonthValue(String date){
        return monthValue[getMonth(date) - 1];
    }

    public static String getMonthValue(Date date){
        return monthValue[getMonth(parseDate(date)) - 1];
    }
}
