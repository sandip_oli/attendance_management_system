package com.phonebook.core.util;

import com.phonebook.core.model.dto.UserDTO;
import com.phonebook.core.model.repository.UserRepository;
import com.phonebook.web.session.UserDetailsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationUtil {

    @Autowired
    private UserRepository userRepository;

    public final Object getCurrentPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        Object principal = authentication.getPrincipal();

        return principal;
    }

    public final com.phonebook.core.model.entity.User getCurrentUserEntity() {

        Object principal = getCurrentPrincipal();

        if (principal instanceof UserDetailsWrapper) {
            UserDTO userDTO = ((UserDetailsWrapper) principal).getUser();
            com.phonebook.core.model.entity.User currentUser = userRepository.findOne(userDTO.getUserId());

            return currentUser;
        }

        return null;
    }

    public final com.phonebook.core.model.entity.User getCurrentUserFullEntity() {

        Object principal = getCurrentPrincipal();

        if (principal instanceof UserDetailsWrapper) {
            UserDTO userDTO = ((UserDetailsWrapper) principal).getUser();
            com.phonebook.core.model.entity.User currentUser = userRepository.findByIdJoinRole(userDTO.getUserId());

            return currentUser;
        }

        return null;
    }

}
