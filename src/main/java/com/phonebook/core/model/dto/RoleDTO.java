package com.phonebook.core.model.dto;

import com.phonebook.core.model.enumconstant.Permission;
import com.phonebook.core.model.enumconstant.Status;
import java.util.Set;

public class RoleDTO {

    private long roleId;

    private String title;

    private Status status;

    private int version;

    private Set<Permission> permissionSet;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<Permission> getPermissionSet() {
        return permissionSet;
    }

    public void setPermissionSet(Set<Permission> permissionSet) {
        this.permissionSet = permissionSet;
    }
}
