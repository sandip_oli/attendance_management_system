package com.phonebook.core.model.dto;

public class StudentInfoDto {

    private Long studentId;
    private String name;
    private Integer rollnum;
    private String address;
    private String mobileNumber;
    private String email;
    private String pastSchool;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRollnum() {
        return rollnum;
    }

    public void setRollnum(Integer rollnum) {
        this.rollnum = rollnum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPastSchool() {
        return pastSchool;
    }

    public void setPastSchool(String pastSchool) {
        this.pastSchool = pastSchool;
    }
}
