package com.phonebook.core.model.dto;

import com.phonebook.core.model.enumconstant.Status;

public class WardInfoDTO {

    private Long wardInfoId;

    private String title;

    private String code;

    private Status status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getWardInfoId() {
        return wardInfoId;
    }

    public void setWardInfoId(Long wardInfoId) {
        this.wardInfoId = wardInfoId;
    }
}
