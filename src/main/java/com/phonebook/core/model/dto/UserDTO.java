package com.phonebook.core.model.dto;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.enumconstant.UserType;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.Set;

public class UserDTO {
	
	private Long userId;

	private String username;

	private String password;

	private String repassword;

	private Set<RoleDTO> roleSet;

	Set<Long> roleIdSet;

	private Status status;

	private String secret_key;

	private boolean enabled;

	private UserType userType;

	private int version;

	private Long branchId;

	private String activation_code;

	private String password_reset_code;

	private boolean password_reset_code_status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date password_reset_request_date;

	public String getActivation_code() {
		return activation_code;
	}

	public void setActivation_code(String activation_code) {
		this.activation_code = activation_code;
	}

	public String getPassword_reset_code() {
		return password_reset_code;
	}

	public void setPassword_reset_code(String password_reset_code) {
		this.password_reset_code = password_reset_code;
	}

	public boolean isPassword_reset_code_status() {
		return password_reset_code_status;
	}

	public void setPassword_reset_code_status(boolean password_reset_code_status) {
		this.password_reset_code_status = password_reset_code_status;
	}

	public Date getPassword_reset_request_date() {
		return password_reset_request_date;
	}

	public void setPassword_reset_request_date(Date password_reset_request_date) {
		this.password_reset_request_date = password_reset_request_date;
	}

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public UserDTO(){
		this.enabled = false;
	}

	public Set<Long> getRoleIdSet() {
		return roleIdSet;
	}

	public void setRoleIdSet(Set<Long> roleIdSet) {
		this.roleIdSet = roleIdSet;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}

	public Set<RoleDTO> getRoleSet() {
		return roleSet;
	}

	public void setRoleSet(Set<RoleDTO> roleSet) {
		this.roleSet = roleSet;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getSecret_key() {
		return secret_key;
	}

	public void setSecret_key(String secret_key) {
		this.secret_key = secret_key;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
