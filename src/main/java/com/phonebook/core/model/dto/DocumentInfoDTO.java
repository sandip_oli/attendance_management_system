package com.phonebook.core.model.dto;

import com.phonebook.core.model.enumconstant.DIR;
import com.phonebook.core.model.enumconstant.Status;

public class DocumentInfoDTO {

    private Long documentInfoId;

    private String title;

    private int size;

    private String name;

    private DIR dir;

    private String dirStr;

    private Status status;

    public Long getDocumentInfoId() {
        return documentInfoId;
    }

    public void setDocumentInfoId(Long documentInfoId) {
        this.documentInfoId = documentInfoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DIR getDir() {
        return dir;
    }

    public void setDir(DIR dir) {
        this.dir = dir;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDirStr() {
        return dirStr;
    }

    public void setDirStr(String dirStr) {
        this.dirStr = dirStr;
    }
}
