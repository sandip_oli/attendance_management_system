package com.phonebook.core.model.dto;

import com.phonebook.core.model.entity.StudentInfo;
import com.phonebook.core.model.enumconstant.AttendanceStatus;

import java.util.Date;

public class AttendanceInfoDto {

    private Long attendanceInfoId;
    private Long studentInfoId;
    private Date date;
    private StudentInfoDto studentInfoDto;
    private String studentName;
    private AttendanceStatus attendanceStatus;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Long getStudentInfoId() {
        return studentInfoId;
    }

    public void setStudentInfoId(Long studentInfoId) {
        this.studentInfoId = studentInfoId;
    }

    public Long getAttendanceInfoId() {
        return attendanceInfoId;
    }

    public void setAttendanceInfoId(Long attendanceInfoId) {
        this.attendanceInfoId = attendanceInfoId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public StudentInfoDto getStudentInfoDto() {
        return studentInfoDto;
    }

    public void setStudentInfoDto(StudentInfoDto studentInfoDto) {
        this.studentInfoDto = studentInfoDto;
    }

    public AttendanceStatus getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(AttendanceStatus attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }
}
