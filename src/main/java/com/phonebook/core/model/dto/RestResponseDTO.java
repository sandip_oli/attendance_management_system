package com.phonebook.core.model.dto;

import com.phonebook.core.model.enumconstant.ResponseStatus;

public class RestResponseDTO {

	private ResponseStatus Status;

	private String message;
	
	Object detail;

	public ResponseStatus getStatus() {
		return Status;
	}

	public void setStatus(ResponseStatus status) {
		Status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getDetail() {
		return detail;
	}

	public void setDetail(Object detail) {
		this.detail = detail;
	}
}
