package com.phonebook.core.model.entity;

import com.phonebook.core.model.enumconstant.AttendanceStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "table_student_attendance")
public class AttendanceInfo extends AbstractEntity<Long>{
    private Date date;
    private AttendanceStatus attendanceStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    private StudentInfo studentInfo;


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AttendanceStatus getAttendanceStatus() {
        return attendanceStatus;
    }

    public void setAttendanceStatus(AttendanceStatus attendanceStatus) {
        this.attendanceStatus = attendanceStatus;
    }

    public StudentInfo getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(StudentInfo studentInfo) {
        this.studentInfo = studentInfo;
    }
}
