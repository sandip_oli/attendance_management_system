package com.phonebook.core.model.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "table_student_info")
public class StudentInfo extends AbstractEntity<Long> {
    private String name;
    private int rollnum;
    private String address;
    private String mobileNumber;
    private String email;
    private String pastSchool;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRollnum() {
        return rollnum;
    }

    public void setRollnum(int rollnum) {
        this.rollnum = rollnum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPastSchool() {
        return pastSchool;
    }

    public void setPastSchool(String pastSchool) {
        this.pastSchool = pastSchool;
    }
}
