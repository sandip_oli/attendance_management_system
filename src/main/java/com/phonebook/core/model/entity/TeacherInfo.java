package com.phonebook.core.model.entity;


import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Table_student_info")

public class TeacherInfo extends AbstractEntity<Long>{
    private String name;

    private String address;

    private String subjectTeacher;

    private String mobileNo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSubjectTeacher() {
        return subjectTeacher;
    }

    public void setSubjectTeacher(String subjectTeacher) {
        this.subjectTeacher = subjectTeacher;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
