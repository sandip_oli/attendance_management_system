package com.phonebook.core.model.entity;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "passwordreset")
public class PasswordReset extends AbstractEntity<Long> {
	
	@ManyToOne(fetch = FetchType.EAGER)
	User user;

	private String passwordResetCode;

	private boolean passwordResetCodeStatus;

	@Temporal(TemporalType.TIMESTAMP)
	private Date passwordResetRequestDate;
	
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPasswordResetCode() {
		return passwordResetCode;
	}

	public void setPasswordResetCode(String passwordResetCode) {
		this.passwordResetCode = passwordResetCode;
	}

	public boolean isPasswordResetCodeStatus() {
		return passwordResetCodeStatus;
	}

	public void setPasswordResetCodeStatus(boolean passwordResetCodeStatus) {
		this.passwordResetCodeStatus = passwordResetCodeStatus;
	}

	public Date getPasswordResetRequestDate() {
		return passwordResetRequestDate;
	}

	public void setPasswordResetRequestDate(Date passwordResetRequestDate) {
		this.passwordResetRequestDate = passwordResetRequestDate;
	}
}
