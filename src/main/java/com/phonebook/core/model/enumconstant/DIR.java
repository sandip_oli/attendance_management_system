package com.phonebook.core.model.enumconstant;

public enum DIR {

    LOGO("logo") , CLIENTDOC("client");

    private final String value;

    DIR(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public static DIR getEnum(String value) {
        if (value == null)
            throw new IllegalArgumentException();
        for (DIR v : values())
            if (value.equalsIgnoreCase(v.getValue()))
                return v;
        throw new IllegalArgumentException();
    }
}
