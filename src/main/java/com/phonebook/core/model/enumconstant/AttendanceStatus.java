package com.phonebook.core.model.enumconstant;

public enum AttendanceStatus {

    PRESENT("p") , LATE("l") , ABSENT("a");

    private final String value;

    AttendanceStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public static AttendanceStatus getEnum(String value) {
        if (value == null)
            throw new IllegalArgumentException();
        for (AttendanceStatus v : values())
            if (value.equalsIgnoreCase(v.getValue()))
                return v;
        throw new IllegalArgumentException();
    }
}
