package com.phonebook.core.model.enumconstant;

public enum Permission {

	USER_CREATE("Role_User_Create") , USER_VIEW("Role_User_View") , USER_UPDATE("Role_User_Update") , USER_DELETE("Role_User_Delete") ,

	REPORT_CREATE("Role_Report_Create") , REPORT_VIEW("Role_Report_View") , REPORT_UPDATE("Role_Report_Update") , REPORT_DELETE("Role_Report_Delete") ,

	ROLE_CREATE("Role_Role_Create") , ROLE_VIEW("Role_Role_View") , ROLE_UPDATE("Role_Role_Update") , ROLE_DELETE("Role_Role_Delete"),

	COUNTRY_CREATE("Role_Country_Create") , COUNTRY_VIEW("Role_Country_View") , COUNTRY_UPDATE("Role_Country_Update") , COUNTRY_DELETE("Role_Country_Delete"),

	STATE_CREATE("Role_State_Create") , STATE_VIEW("Role_State_View") , STATE_UPDATE("Role_State_Update") , STATE_DELETE("Role_State_Delete"),

	CITY_CREATE("Role_City_Create") , CITY_VIEW("Role_City_View") , CITY_UPDATE("Role_City_Update") , CITY_DELETE("Role_City_Delete");

	private final String value;

	Permission(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public static Permission getEnum(String value) {
		if (value == null)
			throw new IllegalArgumentException();
		for (Permission v : values())
			if (value.equalsIgnoreCase(v.getValue()))
				return v;
		throw new IllegalArgumentException();
	}
}
