package com.phonebook.core.model.repository;

import com.phonebook.core.model.entity.StudentInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentInfoRepository  extends JpaRepository<StudentInfo , Long> {

    StudentInfo findById(Long studentid);
}
