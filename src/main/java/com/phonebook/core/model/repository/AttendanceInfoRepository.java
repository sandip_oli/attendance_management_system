package com.phonebook.core.model.repository;


import com.phonebook.core.model.entity.AttendanceInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AttendanceInfoRepository extends JpaRepository<AttendanceInfo, Long > {

    List<AttendanceInfo> findAllByStudentInfo_Id(long studentId);

    List<AttendanceInfo> findAllByDateBetween(Date from , Date to);

    void deleteAllByStudentInfo_Id(long studentId);

}
