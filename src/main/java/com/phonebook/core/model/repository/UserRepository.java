package com.phonebook.core.model.repository;

import com.phonebook.core.model.entity.User;
import com.phonebook.core.model.enumconstant.Status;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

	User findByUsername(String username);

	@Query("select u from User u join fetch u.roleSet where u.username = ?1")
	User findByUsernameWithRole(String username);

	User findByIdAndStatus(long userId , Status status);

	@Query("select u from User u join fetch u.roleSet where u.activationCode = ?1")
	User findByActivationCode(String code);
	
	@Query("select u from User u")
	List<User> findAllUser(Pageable pageable);

	@Query("select u from User u left join fetch u.roleSet")
	List<User> findAllUserJoinRole(Pageable pageable);

	@Query("select u from User u left join fetch u.roleSet where u.id = ?1")
	User findByIdJoinRole(long userId);

	@Query("select count (u) from User u")
	long countAll();

}
