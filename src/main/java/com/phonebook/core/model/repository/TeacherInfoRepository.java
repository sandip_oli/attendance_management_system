package com.phonebook.core.model.repository;


import com.phonebook.core.model.entity.TeacherInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherInfoRepository extends JpaRepository<TeacherInfo , Long>

{

}
