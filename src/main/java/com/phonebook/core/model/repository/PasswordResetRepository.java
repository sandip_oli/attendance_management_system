package com.phonebook.core.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.phonebook.core.model.entity.PasswordReset;

@Repository
public interface PasswordResetRepository extends JpaRepository<PasswordReset , Long> , JpaSpecificationExecutor<PasswordReset>{

	
	@Query("select p from PasswordReset p where p.passwordResetCode=?1")
	PasswordReset findByPasswordResetCode(String code);
}
