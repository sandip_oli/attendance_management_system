package com.phonebook.core.model.repository;

import com.phonebook.core.model.entity.DocumentInfo;
import com.phonebook.core.model.enumconstant.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentInfoRepository extends JpaRepository<DocumentInfo , Long> {

    DocumentInfo findById(long documentId);

    DocumentInfo findByIdAndStatus(long documentId , Status status);
}
