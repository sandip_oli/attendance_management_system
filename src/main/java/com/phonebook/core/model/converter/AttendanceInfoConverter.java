package com.phonebook.core.model.converter;

import com.phonebook.core.model.dto.AttendanceInfoDto;
import com.phonebook.core.model.entity.AttendanceInfo;
import com.phonebook.core.model.entity.StudentInfo;
import com.phonebook.core.model.repository.StudentInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttendanceInfoConverter extends ConverterBase<AttendanceInfo , AttendanceInfoDto> {
   @Autowired
   private StudentInfoConverter studentInfoConverter;

   @Autowired
   private StudentInfoRepository studentInfoRepository;

   @Override
    public AttendanceInfo convertToEntity(AttendanceInfoDto dto) {
        if (dto == null) {
            return super.convertToEntity(dto);
        }
        return copyConvertToEntity(dto , new AttendanceInfo());
    }

    @Override
    public AttendanceInfoDto convertToDto(AttendanceInfo entity) {
        if (entity == null) {
            return super.convertToDto(entity);
        }
        AttendanceInfoDto dto = new AttendanceInfoDto();

        dto.setAttendanceInfoId(entity.getId());
        dto.setAttendanceStatus(entity.getAttendanceStatus());
        dto.setDate(entity.getDate());
        dto.setStudentInfoDto(studentInfoConverter.convertToDto(entity.getStudentInfo()));
        dto.setStudentName(entity.getStudentInfo().getName());
        dto.setStudentInfoId(entity.getStudentInfo().getId());

        return dto;
    }


    @Override
    public AttendanceInfo copyConvertToEntity(AttendanceInfoDto dto, AttendanceInfo entity) {
        if (dto == null || entity == null) {
            return super.copyConvertToEntity(dto, entity);
        }

        entity.setAttendanceStatus(dto.getAttendanceStatus());
        entity.setDate(dto.getDate());
        entity.setStudentInfo(studentInfoRepository.findById(dto.getStudentInfoId()));

        return entity;
    }

    @Override
    public List<AttendanceInfoDto> convertToDtoList(List<AttendanceInfo> entities) {
        return super.convertToDtoList(entities);
    }

    @Override
    public List<AttendanceInfo> convertToEntityList(List<AttendanceInfoDto> dtoList) {
        return super.convertToEntityList(dtoList);
    }

    @Override
    public List<AttendanceInfoDto> convertPageToDtoList(Page<AttendanceInfo> entities) {
        return super.convertPageToDtoList(entities);
    }
}
