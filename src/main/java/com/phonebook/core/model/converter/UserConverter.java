package com.phonebook.core.model.converter;

import com.phonebook.core.model.dto.RoleDTO;
import com.phonebook.core.model.dto.UserDTO;
import com.phonebook.core.model.entity.User;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.repository.RoleRepository;
import com.phonebook.core.util.IConvertable;
import com.phonebook.core.util.IListConvertable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author dhiraj
 *
 */
@Service
public class UserConverter implements IConvertable<User, UserDTO>, IListConvertable<User, UserDTO> {

	@Autowired
	private RoleConverter roleConverter;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public User convertToEntity(UserDTO dto) {

		return copyConvertToEntity(dto , new User());
	}

	@Override
	public UserDTO convertToDto(User entity) {

		if (entity == null) {
			return null;
		}

		UserDTO dto = new UserDTO();

		dto.setUsername(entity.getUsername());
		dto.setStatus(entity.getStatus());
		dto.setUserType(entity.getUserType());
		dto.setUserId(entity.getId());
		dto.setEnabled(entity.isEnabled());
		dto.setVersion(entity.getVersion());
		dto.setActivation_code(entity.getActivationCode());

		return dto;
	}

	public UserDTO convertToDtoWithRoleAndBranch(User entity) {

		UserDTO dto = convertToDto(entity);

		if (dto == null) {
			return null;
		}

		dto.setRoleSet(roleConverter.convertToDtoSet(entity.getRoleSet()));

		return dto;
	}

	public List<UserDTO> convertToDtoListWithRoleAndBranch(List<User> entities) {
		return entities.parallelStream().map(this::convertToDtoWithRoleAndBranch).collect(Collectors.toList());
	}

	@Override
	public User copyConvertToEntity(UserDTO dto , User entity) {

		if (dto == null | entity == null){
			return null;
		}

		entity.setUsername(dto.getUsername().trim().toLowerCase());
		entity.setEnabled(dto.isEnabled());
		entity.setUserType(dto.getUserType());

		if (dto.getRoleIdSet() != null){
			entity.setRoleSet(roleRepository.findAllByStatusAndIdIn(Status.ACTIVE , dto.getRoleIdSet()));
		}

		return entity;
	}

	private List<Long> getRoleIdList(Set<RoleDTO> roleDTOS){

		if (roleDTOS == null){

			return null;
		}

		if (roleDTOS.isEmpty()){
			return null;
		}

		List<Long> longList =new ArrayList<>();

		for (RoleDTO roleDTO : roleDTOS){
			longList.add(roleDTO.getRoleId());
		}

		return longList;
	}

	@Override
	public List<UserDTO> convertToDtoList(List<User> entities) {
		return entities.parallelStream().map(this::convertToDto).collect(Collectors.toList());
	}

	@Override
	public List<User> convertToEntityList(List<UserDTO> dtoList) {
		return dtoList.parallelStream().map(this::convertToEntity).collect(Collectors.toList());
	}

}
