package com.phonebook.core.model.converter;

import com.phonebook.core.model.dto.DocumentInfoDTO;
import com.phonebook.core.model.entity.DocumentInfo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentInfoConverter extends ConverterBase<DocumentInfo , DocumentInfoDTO> {

    @Override
    public DocumentInfo convertToEntity(DocumentInfoDTO dto) {
        return copyConvertToEntity(dto , new DocumentInfo());
    }

    @Override
    public DocumentInfoDTO convertToDto(DocumentInfo entity) {

        if (entity == null) {
            return super.convertToDto(entity);
        }

        DocumentInfoDTO dto = new DocumentInfoDTO();

        dto.setDocumentInfoId(entity.getId());
        dto.setDir(entity.getDir());
        dto.setName(entity.getName());
        dto.setSize(entity.getSize());
        dto.setStatus(entity.getStatus());
        dto.setTitle(entity.getTitle());
        dto.setDirStr(entity.getDir().getValue());

        return dto;
    }

    @Override
    public DocumentInfo copyConvertToEntity(DocumentInfoDTO dto, DocumentInfo entity) {

        if (dto == null || entity == null) {
            return super.copyConvertToEntity(dto, entity);
        }

        entity.setDir(dto.getDir());
        entity.setName(dto.getName().trim());
        entity.setSize(dto.getSize());
        entity.setTitle(entity.getTitle().trim());

        return entity;
    }

    @Override
    public List<DocumentInfoDTO> convertToDtoList(List<DocumentInfo> entities) {
        return super.convertToDtoList(entities);
    }

    @Override
    public List<DocumentInfo> convertToEntityList(List<DocumentInfoDTO> dtoList) {
        return super.convertToEntityList(dtoList);
    }
}
