package com.phonebook.core.model.converter;


import com.phonebook.core.model.dto.TeacherInfoDto;
import com.phonebook.core.model.entity.TeacherInfo;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherInfoConverter  extends ConverterBase<TeacherInfo , TeacherInfoDto>{
    @Override
    public TeacherInfo convertToEntity(TeacherInfoDto dto)
    {
        if (dto == null) {
            return super.convertToEntity(dto);
        }

        return super.convertToEntity(dto);

    }

    @Override
    public TeacherInfoDto convertToDto(TeacherInfo entity) {
        return super.convertToDto(entity);
    }

    @Override
    public TeacherInfo copyConvertToEntity(TeacherInfoDto dto, TeacherInfo entity) {
        return super.copyConvertToEntity(dto, entity);
    }

    @Override
    public List<TeacherInfoDto> convertToDtoList(List<TeacherInfo> entities) {
        return super.convertToDtoList(entities);
    }

    @Override
    public List<TeacherInfo> convertToEntityList(List<TeacherInfoDto> dtoList) {
        return super.convertToEntityList(dtoList);
    }

    @Override
    public List<TeacherInfoDto> convertPageToDtoList(Page<TeacherInfo> entities) {
        return super.convertPageToDtoList(entities);
    }
}
