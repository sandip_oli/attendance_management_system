package com.phonebook.core.model.converter;

import com.phonebook.core.model.dto.RoleDTO;
import com.phonebook.core.model.entity.Role;
import com.phonebook.core.util.IConvertable;
import com.phonebook.core.util.IListConvertable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RoleConverter implements IListConvertable<Role , RoleDTO> , IConvertable<Role , RoleDTO> {

    @Override
    public List<RoleDTO> convertToDtoList(List<Role> entities) {
        return entities.parallelStream().map(this::convertToDto).collect(Collectors.toList());
    }


    public Set<RoleDTO> convertToDtoSet(Set<Role> entities) {

        if (entities == null){
            return null;
        }

        Set<RoleDTO> roleDTOS = new HashSet<>();

        for (Role role : entities){
            roleDTOS.add(convertToDto(role));
        }

        return roleDTOS;
    }

    @Override
    public List<Role> convertToEntityList(List<RoleDTO> dtoList) {
        return dtoList.parallelStream().map(this::convertToEntity).collect(Collectors.toList());
    }

    @Override
    public Role convertToEntity(RoleDTO dto) {
        return copyConvertToEntity(dto , new Role());
    }

    @Override
    public RoleDTO convertToDto(Role entity) {

        if (entity == null) {
            return null;
        }

        RoleDTO dto = new RoleDTO();

        dto.setRoleId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setStatus(entity.getStatus());
        dto.setPermissionSet(entity.getPermissionSet());
        dto.setVersion(entity.getVersion());

        return dto;
    }

    @Override
    public Role copyConvertToEntity(RoleDTO dto, Role entity) {

        if (entity == null | dto == null) {
            return null;
        }

        entity.setTitle(dto.getTitle().trim());
        entity.setPermissionSet(dto.getPermissionSet());

        return entity;
    }
}
