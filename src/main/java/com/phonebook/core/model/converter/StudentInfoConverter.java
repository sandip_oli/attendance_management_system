package com.phonebook.core.model.converter;

import com.phonebook.core.model.dto.StudentInfoDto;
import com.phonebook.core.model.entity.StudentInfo;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentInfoConverter extends ConverterBase<StudentInfo , StudentInfoDto>{
    @Override
    public StudentInfo convertToEntity(StudentInfoDto dto) {
        if (dto == null) {
            return super.convertToEntity(dto);
        }
        return copyConvertToEntity(dto ,new StudentInfo());
    }

    @Override
    public StudentInfoDto convertToDto(StudentInfo entity) {
        if (entity == null){
            return super.convertToDto(entity);
        }
        StudentInfoDto dto = new StudentInfoDto();

        dto.setAddress(entity.getAddress());
        dto.setEmail(entity.getEmail());
        dto.setMobileNumber(entity.getMobileNumber());
        dto.setName(entity.getName());
        dto.setPastSchool(entity.getPastSchool());
        dto.setRollnum(entity.getRollnum());
        dto.setStudentId(entity.getId());

        return dto;
    }

    @Override
    public StudentInfo copyConvertToEntity(StudentInfoDto dto, StudentInfo entity) {

        if (dto == null || entity == null){
            return super.copyConvertToEntity(dto, entity);
        }

        entity.setAddress(dto.getAddress());
        entity.setEmail(dto.getEmail());
        entity.setMobileNumber(dto.getMobileNumber());
        entity.setName(dto.getName());
        entity.setRollnum(dto.getRollnum());
        entity.setPastSchool(dto.getPastSchool());

        return entity;
    }

    @Override
    public List<StudentInfoDto> convertToDtoList(List<StudentInfo> entities) {
        return super.convertToDtoList(entities);
    }

    @Override
    public List<StudentInfo> convertToEntityList(List<StudentInfoDto> dtoList) {
        return super.convertToEntityList(dtoList);
    }

    @Override
    public List<StudentInfoDto> convertPageToDtoList(Page<StudentInfo> entities) {
        return super.convertPageToDtoList(entities);
    }
}
