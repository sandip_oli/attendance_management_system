package com.phonebook.core.validation;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.phonebook.core.api.impl.RecaptchaService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.phonebook.core.model.dto.GeneralMessage;
import com.phonebook.core.model.dto.UserDTO;
import com.phonebook.core.model.entity.PasswordReset;
import com.phonebook.core.model.entity.User;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.enumconstant.UserType;
import com.phonebook.core.model.repository.PasswordResetRepository;
import com.phonebook.core.model.repository.RoleRepository;
import com.phonebook.core.model.repository.UserRepository;
import com.phonebook.web.error.UserError;
import com.phonebook.web.util.LoggerUtil;

/**
 * Created by dhiraj on 5/2/17.
 */
@Service
public class UserValidation extends GlobalValidation {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RecaptchaService recaptchaService;
    
    @Autowired
    private PasswordResetRepository passwordResetRepository;

    UserError error;

    public UserError onSave(UserDTO userDto, BindingResult result) {

        error = new UserError();

        boolean valid = true;

        if (userDto == null) {

            error.setError("invalid request");
            error.setValid(false);

            return error;
        }

        error = validate(result);

        if (!error.isValid()) {
            return error;
        }

        valid = valid && checkUserName(userDto.getUsername());

        valid = valid && checkPassword(userDto.getPassword());

        valid = valid
                && checkRepassword(userDto.getRepassword(),
                userDto.getPassword());

        valid = valid && checkUserType(userDto.getUserType());


        valid = valid && checkRole(userDto.getRoleIdSet());

        error.setValid(valid);

        return error;
    }

    public UserError onUpdate(UserDTO userDto, BindingResult result) {

        error = new UserError();

        boolean valid = true;

        if (userDto == null) {

            error.setError("invalid request");
            error.setValid(false);

            return error;
        }

        error = validate(result);

        if (!error.isValid()) {
            return error;
        }

        valid = valid && checkUser(userDto.getUserId(), userDto.getVersion());

        valid = valid && checkUserType(userDto.getUserType());

        valid = valid && checkRole(userDto.getRoleIdSet());

        error.setValid(valid);

        return error;
    }

    private UserError validate(BindingResult result) {

        if (result.hasErrors()) {

            List<FieldError> errors = result.getFieldErrors();

            for (FieldError errorResult : errors) {

                if (errorResult.getField().equals("userId")) {
                    error.setError("invalid user");
                } else if (errorResult.getField().equals("username")) {
                    error.setUsername("invalid email");
                } else if (errorResult.getField().equals("password")) {
                    error.setPassword("invalid password");
                } else if (errorResult.getField().equals("repassword")) {
                    error.setRepassword("invalid repassword");
                } else if (errorResult.getField().equals("roleSet")) {
                    error.setAuthority("invalid role");
                } else if (errorResult.getField().equals("enabled")) {
                    error.setEnable("invalid active");
                } else if (errorResult.getField().equals("userType")) {
                    error.setUserType("invalid userType");
                }
            }

            error.setValid(false);

            return error;
        }

        error.setValid(true);

        return error;

    }

    private boolean checkUser(long userId, int version) {

        error.setError(checkLong(userId, 0, "user", true));

        if (!("".equals(error.getError()))) {
            return false;
        }

        User user = userRepository.findOne(userId);

        if (user == null) {
            error.setError("user not found");

            return false;
        }

        if (user.getVersion() != version) {

            String link = "<a href='/user/show?userId=" + userId
                    + "'>click</a>";
            error.setError("another user already update this user go back to detail page "
                    + link);

            return false;
        }

        return true;
    }

    private boolean checkUserType(UserType userType) {

        if (userType == null) {

            error.setUserType("userType required");

            return false;
        }

        return true;
    }

    private boolean checkRole(Set<Long> roleIdSet) {

        if (roleIdSet == null) {

            error.setAuthority("role required");

            return false;
        }

        if (roleIdSet.isEmpty()) {

            error.setAuthority("role required");

            return false;
        }

        if (roleIdSet.size() > 20) {

            error.setAuthority("roles must be less than 21");

            return false;
        }

        for (Long value : roleIdSet) {

            error.setAuthority(checkLong(value, 1, "role", true));

            if (!("".equals(error.getAuthority()))) {
                return false;
            }

            if (roleRepository.findByIdAndStatus(value, Status.ACTIVE) == null) {

                error.setAuthority("one of the role not found of id " + value);

                return false;
            }

        }

        return true;
    }

    private boolean checkPassword(String password) {

        error.setPassword(checkString(password, 8, 15, "password", true));

        if (!("".equals(error.getPassword()))) {
            return false;
        }

        return true;
    }

    private boolean checkUserName(String username) {

        error.setUsername(checkEmail(username.trim(), 5, 50, "username", true));

        if (!("".equals(error.getUsername()))) {
            return false;
        }

        if (userRepository.findByUsername(username.trim().toLowerCase()) != null) {

            error.setUsername("username already exist");

            return false;

        }

        return true;
    }

    private boolean checkRepassword(String repassword, String password) {

        error.setRepassword(checkString(repassword, 8, 15, "repassword", true));

        if (!("".equals(error.getRepassword()))) {
            return false;
        }

        if (repassword != null && password != null)

            if (!repassword.equals(password)) {

                error.setRepassword("password did not matched");

                return false;
            }

        return true;
    }

    public boolean validateActivation(String token) {

        boolean valid = true;
        try {
            User u = userRepository.findByActivationCode(token);
            if (!u.getActivationCode().equals(token)) {
                valid = false;
            }

            if (u.isEnabled()) {
                valid = false;
            }

        } catch (Exception e) {
            valid = false;
            LoggerUtil.logWarn(this.getClass(), e);
        }

        return valid;
    }

    public GeneralMessage resetPass(String email, String recaptchaResponse, String remoteIp) {

        GeneralMessage gm = new GeneralMessage();

        try {

            if (recaptchaResponse == null) {
                gm.setValid(false);
                gm.setMessage("request without capcha");
                return gm;

            } else if (recaptchaResponse.isEmpty()) {
                gm.setValid(false);
                gm.setMessage("request without capcha");
                return gm;
            } else {

                String captchaVerifyMessage = recaptchaService.verifyRecaptcha(remoteIp, recaptchaResponse);

                if (StringUtils.isNotEmpty(captchaVerifyMessage)) {
                    gm.setValid(false);
                    gm.setMessage("request invalid capcha");
                    return gm;
                }

            }

            User u = userRepository.findByUsername(email);

            if (u == null){
                gm.setValid(false);
                gm.setMessage("user not found");
                return gm;
            }

            if (u.getStatus() != Status.ACTIVE) {
                gm.setValid(false);
                gm.setMessage("User not Active, Password can't be reset");
                return gm;
            }
            if (!u.isEnabled()) {
                gm.setValid(false);
                gm.setMessage("User not Valid, Password can't be reset");
                return gm;
            }

        } catch (Exception e) {
            LoggerUtil.logWarn(this.getClass(), e);
            gm.setValid(false);
            gm.setMessage("Unknown Error, Contact Operator");
            return gm;
        }


        gm.setValid(true);
        gm.setMessage("");

        return gm;
    }
    
    public GeneralMessage validateResetPass(String code, String password, String rePassword){
    	GeneralMessage gm = resetPassFinal(code);
    	
    	boolean valid= true;
    	
    	if(!gm.isValid()){
    		return gm;
    	}
    	
    	if(password.length()<6 || password.length()>18){
    		gm.setMessage("Password length must between 6 and 18 character");
    		valid= false;
    	}else if(!password.equals(rePassword)){
    		gm.setMessage("Password and repassword doesn't match");
    		valid= false;
    	}
    	gm.setValid(valid);
    	return gm;
    }

    public GeneralMessage resetPassFinal(String code) {

        boolean valid = true;
        String message = "";
        try {
        	PasswordReset pr =passwordResetRepository.findByPasswordResetCode(code);

            if (pr == null) {
                valid = false;
                message = "Password Cannot change. No User Found";
            } else {
                if (!pr.isPasswordResetCodeStatus()) {
                    valid = false;
                    message = "Password Reset already Cancled";
                }

                long diff = new Date().getTime() - pr.getPasswordResetRequestDate().getTime();
                long diffHours = diff / (60 * 60 * 1000);

                if (diffHours >= 24) {
                    valid = false;
                    message = "Password Reset Time Expired";
                }


            }

        } catch (Exception e) {
            LoggerUtil.logWarn(this.getClass(), e);
            valid = false;
            message = "Unknown Error, Contact Operator";
        }
        GeneralMessage gm = new GeneralMessage();
        gm.setValid(valid);
        gm.setMessage(message);

        return gm;
    }
}
