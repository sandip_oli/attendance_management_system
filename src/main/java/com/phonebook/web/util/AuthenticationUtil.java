package com.phonebook.web.util;

import com.phonebook.core.model.dto.UserDTO;
import com.phonebook.web.session.UserDetailsWrapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthenticationUtil {

	public static final UserDTO getCurrentUser() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication == null) {
			return null;
		}

		Object principal = authentication.getPrincipal();

		if (principal instanceof UserDetailsWrapper) {

			UserDTO userDTO = ((UserDetailsWrapper) principal).getUser();

			return userDTO;
		}

		return null;
	}
}
