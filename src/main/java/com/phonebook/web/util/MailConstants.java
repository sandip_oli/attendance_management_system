package com.phonebook.web.util;

public class MailConstants {

    public static final String EXCEPTION_SUBJECT = "Exception Occured";

    public static final String USER_ACTIVATION_SUBJECT = "User Activation Code";

    public static final String MAIL_TITLE= "Title";
    public static final String ACTIVATION_MAIL_TITLE= "Email Verification Request";
    public static final String RESETPASS_MAIL_TITLE= "Reset Password Request";
}
