package com.phonebook.web.util;

public class StringConstants {

	public static final String LASTPAGE = "lastpage";
	public static final String CURRENTPAGE = "currentpage";
	public static final String PAGELIST = "pagelist";

	public static final String ERROR = "error";
	public static final String MESSAGE = "message";
	public static final String STUDENTLIST = "studentlist";
	public static final String STUDENT = "studentInfo";
	public static final String ATTENDANCELIST = "attendancelist";
	public static final String ATTENDANCE = "attendanceinfo";
	public static final String ATTENDANCESTATUSLIST = "attendancestatuslist";

	public static final String STATE_LIST = "stateList";
	public static final String COUNTRY_LIST = "countryList";

	public static final String USER_LIST = "userList";
	public static final String USER = "user";
	public static final String FROM = "from";
	public static final String TO = "to";

	public static final String USER_ERROR = "userError";
	public static final String USER_TYPE_LIST = "userTypeList";

	public static final String ROLE_LIST = "roleList";
	public static final String ROLE = "role";
	public static final String ROLE_ERROR = "roleError";

	public static final String PERMESSION_LIST = "permissionList";

}