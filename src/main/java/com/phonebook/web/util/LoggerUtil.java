package com.phonebook.web.util;

import com.phonebook.core.api.iapi.ISendMailSSL;
import com.phonebook.core.util.MailTemplateUtls;
import org.apache.log4j.Logger;

import java.util.Arrays;

public class LoggerUtil {

	private static Logger LOGGER = Logger.getLogger(LoggerUtil.class);

	public static void logException(Class className,Exception exception){
		LOGGER.error("Exception class "+className);
		LOGGER.error("Error class "+exception.getClass());
		LOGGER.error("Error cause "+exception.getCause());
		LOGGER.error("Error message "+exception.getMessage());
		LOGGER.error("Error stack "+ Arrays.toString(exception.getStackTrace()));

		}

	public static void logException(Class className , Exception exception , ISendMailSSL sendMailSSL){
		logException(className , exception);

		sendMailSSL.sendHtmlMail(ParameterConstants.MAIL_USERNAME , ParameterConstants.EXCEPTION_MAIL_SEND_TO, MailTemplateUtls.exceptionTemplate(className.toString() , exception), MailConstants.EXCEPTION_SUBJECT);
	}

	public static void logWarn(Class className,Exception exception){
		LOGGER.warn("Exception class "+className);
		LOGGER.warn("Error class "+exception.getClass());
		LOGGER.warn("Error cause "+exception.getCause());
		LOGGER.warn("Error message "+exception.getMessage());
	}

	public static void logWarn(Class className, String warning) {
		LOGGER.warn("Warning class " + className);
		LOGGER.warn("Waring " + warning);
	}

	public static void logInfo(Class className, String info){
		LOGGER.warn("Info class " + className);
		LOGGER.warn("Info : " + info);
	}


	public static void logInfo( String info){
		LOGGER.info("Info : " + info);
	}

}
