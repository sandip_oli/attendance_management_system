package com.phonebook.web.util;

import java.io.File;
import java.math.BigDecimal;

public class ParameterConstants {

	public final static String WINDOWS_PATH = System.getProperty( "user.home")+ File.separator+"Documents";
	public final static String MAC_PATH = "/Users";
	public final static String UNIX_PATH = System.getProperty( "user.home");

	public final static String MAIL_USERNAME = "username";
	public final static String EXCEPTION_MAIL_SEND_TO = "dhirajbadu50@gmail.com";
	public final static String MAIL_PASSWORD = "password";

	public final static String EXCEPTION_MAIL_TEMPLATE_PATH = "/mail/exceptionmailtemplate.tag";
	public final static String OFFICIAL_MAIL_TEMPLATE_PATH = "/mail/officialmailtemplate.tag";
}
