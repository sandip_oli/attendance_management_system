package com.phonebook.web.session;


import com.phonebook.core.model.dto.UserDTO;
import com.phonebook.core.model.entity.Role;
import com.phonebook.core.model.entity.User;
import com.phonebook.core.model.enumconstant.Permission;
import com.phonebook.core.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User u = userRepository.findByUsernameWithRole(username.toLowerCase());

		System.out.println("login");

		HttpServletRequest curRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

		final StringBuilder msg = new StringBuilder();

		msg.append(curRequest.getRemoteAddr());

		final String forwardedFor = curRequest.getHeader("X-Forwarded-For");

		if (forwardedFor != null) {
			msg.append(", forwardedFor = ").append(forwardedFor);
		}

		if (u == null) {

			throw new UsernameNotFoundException("user doesnt exists");
		}

        UserDTO dto = getUserDTO(u);

		return new UserDetailsWrapper(dto, getAuthorities(u) , msg.toString());
	}

	private List<GrantedAuthority> getAuthorities(User user){

		Set<String> authorities = new HashSet<>();

		for (Role role : user.getRoleSet()){

			for (Permission permission : role.getPermissionSet()){

				try {

					authorities.add(permission.getValue());

				}catch (Exception e){
					continue;
				}
			}
		}

		List<GrantedAuthority> auth = createAuthorityList(authorities);

		if (auth == null){

			new ArrayList<GrantedAuthority>();

			auth.add(new SimpleGrantedAuthority(user.getUserType().getValue()));

		}else {

			auth.add(new SimpleGrantedAuthority(user.getUserType().getValue()));
		}

		return auth;
	}

	private List<GrantedAuthority> createAuthorityList(Set<String>roles) {

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(roles.size());

		for (String role : roles) {

			authorities.add(new SimpleGrantedAuthority(role));
		}

		return authorities;
	}

	private UserDTO getUserDTO(User user){

		UserDTO dto = new UserDTO();

		dto.setUserId(user.getId());
		dto.setEnabled(user.isEnabled());
		dto.setStatus(user.getStatus());
		dto.setUsername(user.getUsername());
		dto.setPassword(user.getPassword());
		dto.setUserType(user.getUserType());

		return dto;
	}


}
