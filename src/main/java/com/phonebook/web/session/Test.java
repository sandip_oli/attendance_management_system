package com.phonebook.web.session;

public class Test implements Comparable<Test> {

    String date;
    String desc;
    int id;

    public Test(String date, String desc, int id) {
        this.date = date;
        this.desc = desc;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Test{" +
                "date='" + date + '\'' +
                ", desc='" + desc + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

    @Override
    public int compareTo(Test o) {
        int checker = o.date.compareTo(this.date);

        if (checker == 0){
            checker = o.desc.length() - this.desc.length() ;
        }

        return checker;
    }
}
