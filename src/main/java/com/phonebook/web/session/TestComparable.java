package com.phonebook.web.session;

import java.util.ArrayList;
import java.util.Collections;

public class TestComparable {

    public static void main(String[] args) {
        ArrayList<Test> list = new ArrayList<>();
/*        list.add(new Test("2018-01-01", "Test1", 1));
       list.add(new Test("", "Test2", 2));
       list.add(new Test("2018-03-01", "Test3", 3));
       list.add(new Test("2018-01-01", "Testsadfjkls4", 4)); */

        list.add(new Test("", "Test1", 1));
        list.add(new Test("", "Tset1", 4));
        list.add(new Test("", "Test12Test12dkgjasdglkjdl", 2));
        list.add(new Test("", "Test12dkgjasdglkjdl;kajgkl3", 3));


        Collections.sort(list);
        System.out.println("list.get(0) = " + list.get(0));
        for (Test t:list) {
            System.out.println(t);
        }

    }
}
