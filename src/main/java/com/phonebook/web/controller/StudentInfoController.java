package com.phonebook.web.controller;

import com.phonebook.core.api.iapi.IStudentAttendenceApi;
import com.phonebook.core.api.iapi.IStudentInfoApi;
import com.phonebook.core.model.dto.AttendanceInfoDto;
import com.phonebook.core.model.dto.StudentInfoDto;
import com.phonebook.web.util.LoggerUtil;
import com.phonebook.web.util.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping("/student")
public class StudentInfoController {

    @Autowired
    private IStudentInfoApi studentInfoApi;

    @Autowired
    private IStudentAttendenceApi studentAttendenceApi;

    @GetMapping("/list")
    public String list(ModelMap map)
    {
        List<StudentInfoDto> studentInfoDtoList = studentInfoApi.list();
        map.put(StringConstants.STUDENTLIST , studentInfoDtoList);
        return "student/list";
    }

    @GetMapping("/add")
    public String add()
    {
        return "student/add";
    }


    @GetMapping("/edit")
    public String edit(@RequestParam("studentId")Long studentId , RedirectAttributes redirectAttributes , ModelMap map)
    {
        try {
            StudentInfoDto studentInfoDto = studentInfoApi.show(studentId);

            if (studentInfoDto == null){
                redirectAttributes.addFlashAttribute(StringConstants.ERROR , "student not found");
                return "redirect:student/list";
            }
            map.put(StringConstants.STUDENT , studentInfoDto);
        }catch (Exception ex){
            LoggerUtil.logException(this.getClass() , ex);
            throw ex;
        }
        return "student/edit";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("studentId")Long studentId , RedirectAttributes redirectAttributes , ModelMap map)
    {
        try {
            synchronized (this.getClass()) {

                StudentInfoDto studentInfoDto = studentInfoApi.show(studentId);

                if (studentInfoDto == null) {
                    redirectAttributes.addFlashAttribute(StringConstants.ERROR, "student not found");
                    return "redirect:/student/list";
                }
                studentInfoApi.delete(studentId);
                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE, "student delete Successfully.");
            }
        }catch (Exception ex){
            LoggerUtil.logException(this.getClass() , ex);
            throw ex;
        }
        return "redirect:/student/list";
    }



    @PostMapping("/save")
    public String save(@ModelAttribute("student")StudentInfoDto studentInfoDto , RedirectAttributes redirectAttributes){
        try {
            synchronized (this.getClass()) {
                studentInfoApi.save(studentInfoDto);
                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE , "Student Added Sucessfully.");
            }
        }catch (Exception e){
            LoggerUtil.logException(this.getClass(), e);
            throw e;
        }
        return "redirect:/student/list";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("student")StudentInfoDto studentInfoDto , RedirectAttributes redirectAttributes){
        try {
            synchronized (this.getClass()) {
                studentInfoApi.update(studentInfoDto);
                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE , "Student Updated Sucessfully.");
            }
        }catch (Exception e){
            LoggerUtil.logException(this.getClass(), e);
            throw e;
        }
        return "redirect:/student/list";
    }

    @GetMapping("/show")
    public String show(@RequestParam("studentId")Long studentId , RedirectAttributes redirectAttributes , ModelMap map){
        try{
            if (studentId==null){
                redirectAttributes.addFlashAttribute(StringConstants.ERROR , "student not found");
                return "redirect:student/list";
            }
            StudentInfoDto studentInfoDto = studentInfoApi.show(studentId);
            if (studentInfoDto==null){
                redirectAttributes.addFlashAttribute(StringConstants.ERROR , "student not found");
                return "redirect:student/list";
            }
            List<AttendanceInfoDto> attendanceInfoDtoList = studentAttendenceApi.getAllByStudentId(studentId);
            map.put(StringConstants.STUDENT , studentInfoDto);
            map.put(StringConstants.ATTENDANCELIST , attendanceInfoDtoList);

        }catch (Exception e){
            LoggerUtil.logException(this.getClass(), e);
            throw e;
        }
        return "student/show";
    }

}
