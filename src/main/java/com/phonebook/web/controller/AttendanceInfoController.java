package com.phonebook.web.controller;


import com.phonebook.core.api.iapi.IStudentAttendenceApi;
import com.phonebook.core.api.iapi.IStudentInfoApi;
import com.phonebook.core.model.dto.AttendanceInfoDto;
import com.phonebook.core.model.enumconstant.AttendanceStatus;
import com.phonebook.web.util.LoggerUtil;
import com.phonebook.web.util.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/attendance")
public class AttendanceInfoController {

    @Autowired
    private IStudentAttendenceApi studentAttendenceApi;

    @Autowired
    private IStudentInfoApi studentInfoApi;

    @GetMapping("/list")
    public String list(ModelMap map)
    {
        List<AttendanceInfoDto> studentInfoDtoList = studentAttendenceApi.list();
        map.put(StringConstants.ATTENDANCELIST , studentInfoDtoList);
        return "attendance/list";
    }

    @GetMapping("/add")
    public String add(ModelMap map) {
            map.put(StringConstants.STUDENTLIST , studentInfoApi.list());
            map.put(StringConstants.ATTENDANCESTATUSLIST , AttendanceStatus.values());
            return "attendance/add";


    }

    @PostMapping("/save")
    public String save(@ModelAttribute("attendance")AttendanceInfoDto attendanceInfoDto){
        try {
            studentAttendenceApi.save(attendanceInfoDto);
        }catch (Exception e){
            LoggerUtil.logException(this.getClass() , e);
        }
        return "redirect:/attendance/list";
    }



}
