package com.phonebook.web.controller;

import com.phonebook.core.api.iapi.IStateInfoApi;
import com.phonebook.web.util.StringConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/state")
public class StateController {
	
	@Autowired
	private IStateInfoApi stateService;

	@PreAuthorize("hasRole('Role_State_View')")
	@GetMapping("/list")
	public String list(ModelMap modelMap){

		modelMap.put(StringConstants.STATE_LIST , stateService.list());

		return "state/list";
	}

}
