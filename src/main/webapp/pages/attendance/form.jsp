<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Student</label>
                <select class="form-control" name="studentInfoId" placeholder="Name" required>
                    <c:forEach items="${studentlist}" var="student">
                        <option value="${student.studentId}">${student.name}</option>
                    </c:forEach>

                </select>
                <p class="form-error">${studentInfoError.studentInfoId}</p>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Student Status</label>
                <select class="form-control" name="attendanceStatus" placeholder="Name" required>
                    <c:forEach items="${attendancestatuslist}" var="status">
                        <option value="${status}">${status}</option>
                    </c:forEach>

                </select>
                <p class="form-error">${studentInfoError.status}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Date *</label>
                <input type="text" class="form-control datepicker" onkeypress="return false;" onkeyup="return false;" name="date" placeholder="From Date" required>
                <p class="form-error">${clientUnitInfoError.fromDate}</p>
            </div>
        </div>
    </div>



</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

