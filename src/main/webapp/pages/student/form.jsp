<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Name *</label>
                <input type="text" class="form-control" value="${studentInfo.name}" name="name" placeholder="Name" required>
                <p class="form-error">${studentInfoError.name}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Roll No *</label>
                <input type="number" class="form-control" value="${studentInfo.rollnum}" name="rollnum" placeholder="Enter Your Roll No" required>
                <p class="form-error">${studentInfoEroor.rollnum}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Address *</label>
                <input type="text" class="form-control" value="${studentInfo.address}" name="address" placeholder="Enter Your Address" required>
                <p class="form-error">${studentInfoEroor.address}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Mobile No *</label>
                <input type="number" class="form-control" value="${studentInfo.mobileNumber}" name="mobileNumber" placeholder="Enter Your Mobile Number" required>
                <p class="form-error">${studentInfoEroor.mobileNumber}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Email *</label>
                <input type="email" class="form-control" value="${studentInfo.email}" name="email" placeholder="Enter Your Email" required>
                <p class="form-error">${studentInfoEroor.email}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Past School *</label>
                <input type="text" class="form-control" value="${studentInfo.pastSchool}" name="pastSchool" placeholder="Enter Your Past School" required>
                <p class="form-error">${studentInfoEroor.pastSchool}</p>
            </div>
        </div>
    </div>




</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

