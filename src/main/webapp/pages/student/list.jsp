<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="Student">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Student Info List</h3>

                                <a href="${pageContext.request.contextPath}/student/add" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="">
                                            <table id="table3" class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Roll No</th>
                                                    <th>Address</th>
                                                    <th>Mobile Number</th>
                                                    <th>Email</th>
                                                    <th>Past School</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody id="myData">
                                                <c:forEach items="${studentlist}" var="studentinfo">
                                                    <tr>
                                                        <td> <a href="${pageContext.request.contextPath}/student/show?studentId=${studentinfo.studentId}" class="btn btn-default btn-sm">${studentinfo.studentId}</a></td>
                                                        <td><a href="${pageContext.request.contextPath}/student/show?studentId=${studentinfo.studentId}" >${studentinfo.name}</a></td>
                                                        <td>${studentinfo.rollnum}</td>
                                                        <td>${studentinfo.address}</td>
                                                        <td>${studentinfo.mobileNumber}</td>
                                                        <td>${studentinfo.email}</td>
                                                        <td>${studentinfo.pastSchool}</td>
                                                        <td>
                                                            <a href="${pageContext.request.contextPath}/student/edit?studentId=${studentinfo.studentId}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                                            <a href="${pageContext.request.contextPath}/student/delete?studentId=${studentinfo.studentId}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
                                                        </td>

                                                    </tr>
                                                </c:forEach>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>