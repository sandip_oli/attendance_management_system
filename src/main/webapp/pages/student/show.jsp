<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="Student">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Student Info Detail</h3>

                                <a href="${pageContext.request.contextPath}/student/list" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-list"></span>&nbsp; List</a>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Roll No</th>
                                                    <th>Address</th>
                                                    <th>Mobile Number</th>
                                                    <th>Email</th>
                                                    <th>Past School</th>

                                                </tr>
                                                </thead>
                                                <tbody id="myData">

                                                    <tr>
                                                        <td>${studentInfo.studentId}</td>
                                                        <td>${studentInfo.name}</td>
                                                        <td>${studentInfo.rollnum}</td>
                                                        <td>${studentInfo.address}</td>
                                                        <td>${studentInfo.mobileNumber}</td>
                                                        <td>${studentInfo.email}</td>
                                                        <td>${studentInfo.pastSchool}</td>


                                                    </tr>


                                                </tbody>
                                            </table>
                                            <table id="table3" class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Date</th>
                                                    <th>Attendance Status</th>
                                                </tr>
                                                </thead>
                                                <tbody id="myData">
                                                <c:forEach items="${attendancelist}" var="attendance">
                                                    <tr>
                                                        <td>${attendance.attendanceInfoId}</td>
                                                        <td>${attendance.studentName}</td>
                                                        <td>${attendance.date}</td>
                                                        <td>${attendance.attendanceStatus}</td>

                                                    </tr>
                                                </c:forEach>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>