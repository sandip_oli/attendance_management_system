<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Title *</label>
                <input type="text" class="form-control" value="${role.title}" name="title" placeholder="title" required>
                <p class="form-error">${roleError.title}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Permission *</label>
                <select name="permissionSet" class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Select Permissions" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach items="${permissionList}" var="permission">
                        <c:choose>
                            <c:when test="${fn:length(role.permissionSet) gt 0}">
                                <spring:eval var="containsValue" expression="role.permissionSet.contains(permission)" />
                            </c:when>
                            <c:otherwise>
                                <c:set var="containsValue" value="${false}"></c:set>
                            </c:otherwise>
                        </c:choose>
                           <c:choose>
                                <c:when test="${containsValue}">
                                    <option selected="selected">${permission}</option>
                                </c:when>
                                <c:otherwise>
                                    <option>${permission}</option>
                                </c:otherwise>
                            </c:choose>
                    </c:forEach>
                </select>
            </div>
            <p class="form-error">${roleError.permission}</p>
        </div>
    </div>

</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

