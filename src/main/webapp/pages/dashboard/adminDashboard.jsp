<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>${totalUnit}</h3>

                <p>Total Unit</p>
            </div>
            <div class="icon">
                <i class="ion ion-cube"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><fmt:formatNumber type="number" maxFractionDigits="2" groupingUsed="true" value="${totalAmount}"/></h3>

                <p>Total Amount</p>
            </div>
            <div class="icon">
                <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>${thisYearUnit}</h3>

                <p>Year Unit</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>${totalUser}</h3>

                <p>Total Users</p>
            </div>
            <div class="icon">
                <i class="ion ion-person-add"></i>
            </div>
            <a href="${pageContext.request.contextPath}/user/list" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">This year's Monthly Unit Report</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <%--<div class="btn-group">
                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>--%>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            <%--  <strong>1 Jan, 2017 - 30 Dec, 2017</strong>--%>
                        </p>

                        <div class="chart">
                            <!-- Sales Chart Canvas -->
                            <canvas id="salesChart" style="height: 250px;"></canvas>
                        </div>
                        <!-- /.chart-responsive -->
                    </div>
                    <!-- /.col -->
                    <%--<div class="col-md-4">
                        <p class="text-center">
                            <strong>Sales Order Status</strong>
                        </p>

                        <div class="progress-group">
                            <span class="progress-text">Order Pending</span>
                            <span class="progress-number"><b>${totalPendingSale}</b></span>

                            <div class="progress sm">
                                <div class="progress-bar progress-bar-primary" style="width: 80%"></div>
                            </div>
                        </div>

                        <div class="progress-group">
                            <span class="progress-text">Order Accepted</span>
                            <span class="progress-number"><b>${totalAcceptedSale}</b></span>

                            <div class="progress sm">
                                <div class="progress-bar label-default" style="width: 80%"></div>
                            </div>
                        </div>

                        <!-- /.progress-group -->
                        <div class="progress-group">
                            <span class="progress-text">Order Packed</span>
                            <span class="progress-number"><b>${totalPackedSale}</b></span>

                            <div class="progress sm">
                                <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                            </div>
                        </div>
                        <!-- /.progress-group -->
                        <div class="progress-group">
                            <span class="progress-text">Order Shipped</span>
                            <span class="progress-number"><b>${totalShipedSale}</b></span>

                            <div class="progress sm">
                                <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                            </div>
                        </div>
                        <!-- /.progress-group -->
                        <div class="progress-group">
                            <span class="progress-text">Order Cancelled</span>
                            <span class="progress-number"><b>${totalCanceledSale}</b></span>

                            <div class="progress sm">
                                <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                            </div>
                        </div>
                        <!-- /.progress-group -->
                    </div>--%>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
               <%-- <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            &lt;%&ndash; <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 17%</span>&ndash;%&gt;
                            <h5 class="description-header">$<fmt:formatNumber type="number" maxFractionDigits="3" groupingUsed="true" value="${totalPayment}"/></h5>
                            <span class="description-text">TOTAL COLLECTION</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            &lt;%&ndash; <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>&ndash;%&gt;
                            <h5 class="description-header">$<fmt:formatNumber type="number" maxFractionDigits="3" groupingUsed="true" value="${totalReceivable}"/></h5>
                            <span class="description-text">TOTAL RECIEVABLE</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block border-right">
                            &lt;%&ndash;<span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>&ndash;%&gt;
                            <h5 class="description-header">$<fmt:formatNumber type="number" maxFractionDigits="3" groupingUsed="true" value="${toDayTotalSale}"/></h5>
                            <span class="description-text">TODAYS SALE</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="description-block">
                            &lt;%&ndash;<span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>&ndash;%&gt;
                            <h5 class="description-header">$<fmt:formatNumber type="number" maxFractionDigits="3" groupingUsed="true" value="${toDayTotalPayment}"/></h5>
                            <span class="description-text">TODAYS COLLECTION</span>
                        </div>
                        <!-- /.description-block -->
                    </div>
                </div>--%>
                <!-- /.row -->
            </div>
            <!-- /.box-footer -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Client List</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="">
                    <table id="ajaxDataTable" url="${pageContext.request.contextPath}/ajax/clientinfo/list" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Street</th>
                        </tr>
                        </thead>
                        <tbody id="myData">

                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="${pageContext.request.contextPath}/clientapplication/add" class="btn btn-sm btn-primary btn-flat pull-left">Add</a>
                <a href="${pageContext.request.contextPath}/clientinfo/list" class="btn btn-sm btn-default btn-flat pull-right">View All Client</a>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
</div>