<div class="row">
    <div class="col-md-12">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Client List</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="">
                    <table id="ajaxDataTable" url="${pageContext.request.contextPath}/ajax/clientinfo/list" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>SN</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Street</th>
                        </tr>
                        </thead>
                        <tbody id="myData">

                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a href="${pageContext.request.contextPath}/clientinfo/add" class="btn btn-sm btn-primary btn-flat pull-left">Add</a>
                <a href="${pageContext.request.contextPath}/clientinfo/list" class="btn btn-sm btn-default btn-flat pull-right">View All Client</a>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
</div>