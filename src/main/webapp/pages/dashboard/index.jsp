<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="dashboard">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Attendance Report</h3>

                                <a href="${pageContext.request.contextPath}/attendance/add"
                                   class="btn btn-info btn-sm btn-flat pull-right"><span
                                        class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <form action="${pageContext.request.contextPath}/dashboard" autocomplete="off"
                                          method="get">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">From Date *</label>
                                                <input type="text" class="form-control datepicker"
                                                       onkeypress="return false;" onkeyup="return false;" name="from"
                                                       placeholder="From Date" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${from}"/>" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">To Date *</label>
                                                <input type="text" class="form-control datepicker"
                                                       onkeypress="return false;" onkeyup="return false;" name="to"
                                                       placeholder="To Date" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${to}"/>" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">Filter</label>
                                                <button type="submit" class="form-control btn btn-success"><i
                                                        class="fa fa-filter"></i> Filter
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="">
                                            <table id="table3" class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Date</th>
                                                    <th>Attendance Status</th>
                                                </tr>
                                                </thead>
                                                <tbody id="myData">
                                                <c:forEach items="${attendancelist}" var="attendance">
                                                    <tr>
                                                        <td>${attendance.attendanceInfoId}</td>
                                                        <td>${attendance.studentName}</td>
                                                        <td>${attendance.date}</td>
                                                        <td>${attendance.attendanceStatus}</td>

                                                    </tr>
                                                </c:forEach>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>