<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!-- Main Footer -->
<footer class="main-footer">

    <strong>Copyright &copy; <%=new java.text.SimpleDateFormat("yyyy").format(new java.util.Date())%> <a href="#"><b>SSR</a>.</strong> All rights reserved.
</footer>

<aside class="control-sidebar control-sidebar-dark">

    <!-- Create the tabs -->
    <jsp:include page="/pages/parts/setting/setting_menu.jsp"/>
    <!-- Tab panes -->
    <%--<div class="tab-content">--%>
        <!-- Home tab content -->
       <%-- <div class="tab-pane active" id="control-sidebar-home-tab">

            <jsp:include page="/pages/parts/setting/setting_home.jsp"/>

        </div>--%>

        <div class="tab-pane active" id="control-sidebar-settings-tab">

            <jsp:include page="/pages/parts/setting/setting_stats.jsp"/>

        </div>

   <%-- </div>--%>
    <!-- /.tab-pane -->
</aside>

<div class="control-sidebar-bg"></div>

<!-- ./wrapper which was started in header page-->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<!-- Slim Scroll -->
<script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.min.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/js/adminlte.min.js"></script>
<!-- select2-->
<script src="${pageContext.request.contextPath}/resources/js/select2.full.min.js"></script>
<!-- icheck-->
<script src="${pageContext.request.contextPath}/resources/js/icheck.min.js"></script>
<!-- datatable-->
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<!-- custom script-->
<script src="${pageContext.request.contextPath}/resources/js/assest/app/custom_app.js"></script>
<!-- datepicker-->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap3-wysihtml5-->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap3-wysihtml5.all.min.js"></script>
<%--number validation--%>
<script src="${pageContext.request.contextPath}/resources/js/assest/numberValidation.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/assest/print.js"></script>
<%--confirm delete--%>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap-confirm-delete.js"></script>

<%--jquery validation--%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>