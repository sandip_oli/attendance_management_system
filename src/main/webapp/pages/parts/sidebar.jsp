<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>


<aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- Sidebar user panel (optional) -->
		<div class="user-panel">
			<div class="pull-left image">
				<img
					src="${pageContext.request.contextPath}/resources/img/user2-160x160.jpg"
					class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>${pageContext.request.userPrincipal.name}</p>
				<!-- Status -->
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- Sidebar Menu -->
<%--			<style type="text/css">

				#style-4::-webkit-scrollbar-track
				{
					-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
					background-color: #F5F5F5;
				}

				#style-4::-webkit-scrollbar
				{
					width: 10px;
					background-color: #333333;

				}

				#style-4::-webkit-scrollbar-thumb
				{
					background-color: #000000;
					border: 2px solid #555555;
				}

				/* Handle on hover */
				::-webkit-scrollbar-thumb:hover {
					background: #555;
					cursor: col-resize;
				}

			</style>--%>
			<ul class="sidebar-menu" data-widget="tree">

				<jsp:include page="/pages/parts/sidebar/sidebar-system.jsp" />

			</ul>
		<!-- /.sidebar-menu -->
	</section>
	<!-- /.sidebar -->
</aside>
