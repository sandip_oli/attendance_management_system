<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 9/29/18
  Time: 2:51 PM
  To change this template use File | Settings | File Templates.
--%>


<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<ul class="sidebar-menu tree" data-widget="tree">
    <sec:authorize access="hasAnyRole('System' , 'Admin')">
        <li><a href="${pageContext.request.contextPath}/clienttype/list"><i class="fa fa-yen"></i>
            <span>ClientType</span></a></li>
    </sec:authorize>
</ul>