var request ;
function currencyService() {

    return {

        ajaxPost : function (url , data) {
            request = $.ajax({
                type: "POST",
                url: url,
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: data,
                dataType: 'json',
                timeout: 30000,
                tryCount : 0,
                retryLimit : 3,
                beforeSend: function() {
                    // setting a timeout
                    if (request !== undefined) {
                        request.abort();
                    }
                },
                success: function (data) {
                    
                    var response = new restResponse();
                    
                    response.data = data;
                    response.message = "200";
                    response.status = "200";
                    
                    return response
                },
                error : function(xhr, textStatus, errorThrown ) {
                    console.log(xhr + " " + textStatus + " " + errorThrown);

                    if (textStatus === 'timeout') {

                        this.tryCount++;

                        if (this.tryCount <= this.retryLimit) {
                            //try again
                            $.ajax(this);
                        } else {
                            //cancel request
                        }

                    }

                    if (xhr.status === 500) {
                        //handle error
                        //window.location.reload();
                    } else {
                        //handle error
                        //window.location.reload();
                    }

                    var response = new restResponse();

                    response.data = null;
                    response.message = "unable to make connection";
                    response.status = "500";

                    return response
                }
            });
        },
        
        save : function (currency, url) {

            var response = this.ajaxPost(url , currency);

            if (response.status === "500"){
                alert(response.message)
            }

            if (response.status === "200"){

                var result = response.data.data.detail;

                var msg = response.data.data.message;

                var status = response.data.data.status;

                if (status === 'Success') {
                    this.saveSuccessHandler(result);
                }

                if (status === 'Failure') {
                    console.log(response);
                }

                if (status === 'Validation Failed') {
                    console.log(response);
                }
            }
        },

        saveSuccessHandler : function (data) {
            console.log(response);
        }
    }

}