
var currencyService = new currencyService();

function saveCurrency(url) {

    var currency = new currency();

    currency = collectForm(currency);

    currencyService.save(currency , url);
}

function collectForm(currency) {

    var name = $("#name").val();
    var country = $("#country").val();

    if(name === undefined){
        name = "";
    }

    if(country === undefined){
        country = "";
    }

    currency.currencyName = name;
    currency.countryId = country;

    return currency;
}
