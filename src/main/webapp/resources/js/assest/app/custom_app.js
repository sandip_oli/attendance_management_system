
$(document).ready(function () {

    $(document).on("change" , "select[name='userType']" , function () {

        var value = $(this).val();

        console.log(value);

        if (value === "BRANCH"){
            $("#user_branch_row").removeClass("hidden").removeClass("show").addClass("show");
        }else {
            $("#user_branch_row").removeClass("hidden").removeClass("show").addClass("hidden");
        }
    })
});