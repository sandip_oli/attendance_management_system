<%@ attribute name="title" required="true" %>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${title}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/skin-blue.min.css">
    <%--icheckbox--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/blue.css">
    <!-- select2 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/select2.css">
    <!-- custom -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/custom.css">
    <!-- datatable -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.dataTables.min.css">
    <!-- bootstrap3-wysihtml5 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap3-wysihtml5.min.css">
    <!-- date picker -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap-confirm-delete.css">
<%--fav icon start--%>
    <link rel="apple-touch-icon" sizes="57x57" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="${pageContext.request.contextPath}/resources/images/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/resources/images/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/images/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/resources/images/fav/favicon-16x16.png">
    <link rel="manifest" href="${pageContext.request.contextPath}/resources/images/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/images/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
<%--fav icon end--%>
   <%-- <link rel="icon" href="${pageContext.request.contextPath}/resources/images/fav.png" sizes="32x32"
          type="image/x-icon">--%>
    <%--
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--%>

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <style>
        .my-modal .modal {
            position: relative;
            top: auto;
            bottom: auto;
            right: auto;
            left: auto;
            display: block;
            z-index: 1;
        }

        .my-modal .modal {
            background: transparent !important;
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini fixed">
    <div class="wrapper">
        <jsp:include page="/pages/parts/header.jsp"/>

        <jsp:include page="/pages/parts/sidebar.jsp"/>

        <div id="body">
            <jsp:doBody/>
        </div>

        <jsp:include page="/pages/parts/footer.jsp"/>
    </div>
<script>

    var pageContext = '${pageContext.request.contextPath}';
    $(document).ready(function () {
        $('.confirmDelete').bootstrap_confirm_delete({});

        //$(".content-wrapper").attr("style" , "style='min-height: 100%;'");

        $(document).on("click" , ".treeview" , function () {
            console.log("you clicked on sidebar");
            $(".scroll-style-4").attr("style" , "height: 100%; overflow-y: scroll ;");
        });

        $('.select2').select2();

        $(function () {
            $.fn.dataTable.ext.errMode = 'none';
            $('#ajaxTable').on('error.dt', function (e, settings, techNote, message) {
                console.log('An error has been reported by DataTables: ', message);
            }).DataTable({
                "processing": true,
                "serverSide": true,
                'searching': false,
                'ordering': true,
                'lengthChange': true,
                "ajax": $('#ajaxTable').attr('url')
            });

            $('.datepicker').datepicker({
                autoclose: true,
                todayHighlight: true
            });

            $(".datepickermonth").datepicker( {
                autoclose: true,
                todayHighlight: true,
                minViewMode: "months"
            });

            //bootstrap WYSIHTML5 - text editor
            $('.editor').wysihtml5();

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

    });
</script>

<%--for sidebar active--%>
<script>
    /** add active class and stay opened when selected */
    var url = window.location;

    if (url === undefined){
        url = "none";
    }

    $(function () {
        // for sidebar menu entirely but not cover treeview
        $('ul.sidebar-menu a').filter(function () {
            return this.href == url;
        }).parent().siblings().removeClass('active').end().addClass('active');

        // for treeview
        $('ul.treeview-menu a').filter(function () {
            return this.href == url;
        }).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active').end().addClass('active');
    });
</script>

<script>
    $(function () {
        $('#table1').DataTable();
        $('#table2').DataTable({
            'paging': false,
            'lengthChange': false,
            'searching': true,
            'ordering': true,
            'info': false,
            'autoWidth': true
        });

        $('#table3').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': false,
            'autoWidth': true
        });
    })
</script>

<script>
    $("#currencyForm").validate({
        focusInvalid: true,
        rules: {
            currency: {
                required: true,
                maxlength: 10,
                minlength: 1
            },
            currenceyInfoId: {
                required: true,
                min: 1
            }
        },
        messages: {
            currency: {
                required: "currency is required field",
                maxlength: "currency must be smaller than 10 charactor",
                minlength: "please press any key"
            },
            currenceyInfoId: {
                required: "country is required field",
                min: "please select any country"
            }
        },

        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },

        submitHandler: function (form, event) {

            alert("Submitted!");
            //saveCurrency("${pageContext.request.contextPath}/currency/save");
            event.preventDefault();
        }

    });

</script>

</body>
</html>